﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static UnityEngine.Mathf;

/// <summary>
/// Intermediary class to hold states of generation in room generatoion
/// </summary>
[System.Serializable]
public class GeneratingRoom
{
    private GameObject root;
    public Transform Root => root.transform;
    /// <summary>
    /// Initialize a new room to be generated
    /// </summary>
    /// <param name="name"></param>
    public GeneratingRoom(string name)
    {
        root = new GameObject(name, typeof(Room));
#if UNITY_EDITOR
        UnityEditor.Undo.RegisterCreatedObjectUndo(root, "Created Room"); //As we're just mocking it for now, lets enable undo-ing
#endif
        allTiles    = new HashSet<Vector2Int>();
        floorTiles  = new HashSet<Vector2Int>();
        wallTiles   = new HashSet<Vector2Int>();

        northWalls = new HashSet<Vector2Int>();
        southWalls = new HashSet<Vector2Int>();
        eastWalls = new HashSet<Vector2Int>();
        westWalls = new HashSet<Vector2Int>();

        cornerTiles = new HashSet<Vector2Int>();
    }
    public RoomType roomType;
    public RoomType.RoomSet tileSet;

    /// <summary>
    /// ALL THE TILE POSITIONS IN THE ROOM
    /// NOTE: ALL OF THEM ARE IN TILESPACE
    /// </summary>
    public HashSet<Vector2Int> allTiles;
    /// <summary>
    /// ALL WALL TILES OF THE ROOM
    /// NOTE: ALL OF THEM ARE IN TILESPACE
    /// </summary>
    public HashSet<Vector2Int> wallTiles;
    /// <summary>
    /// ALL FLOOR TILES OF THE ROOM
    /// NOTE: ALL OF THEM ARE IN TILESPACE
    /// </summary>
    public HashSet<Vector2Int> floorTiles;
    /// <summary>
    /// ALL CORNER TILES OF THE ROOM
    /// NOTE: ALL OF THEM ARE IN TILESPACE
    /// </summary>
    public HashSet<Vector2Int> cornerTiles;

    public HashSet<Vector2Int> northWalls;
    public HashSet<Vector2Int> southWalls;
    public HashSet<Vector2Int> eastWalls;
    public HashSet<Vector2Int> westWalls;

    [SerializeField] private RectInt bounds;
    public RectInt Bounds => bounds;
    //Position in finalized space
    public Vector2Int Position => bounds.position;

    public Vector2Int MaterializedPositionOffset { get; private set; }

    /*
     
        During generation there are four spaces to consider:
        unit space tile positioning - all tiles get a position in a grid of units of size 1 unit
        tile space tile positioning - same as above, but whith the positions multiplied with tile size, i.e. 2
        materialized space - the room transform is moved in order to match origin with the calculated bounds
            Calculations for where the room can fit is then done, resulting in a new origin
        finalized space - the tiles are combined and bound to the room transform, the room is then moved to the moved bounds origin
             
    */



    private bool materialized;
    private bool finalized;
    public bool Finalized => finalized;

    public struct RoomLayout
    {
        public GameObject[] tiles;
        public Vector2Int[] tilePositions;
        public RoomLayout(int tileCount)
        {
            tiles           = new GameObject[tileCount];
            tilePositions   = new Vector2Int[tileCount];
        }
    }
    public RoomLayout roomLayout;
    public Vector2Int MaterializedSpace( Vector2Int tilePosition ) => MaterializedPositionOffset + tilePosition * 2;
    public Vector3 Materialized3DSpace( Vector2Int tilePosition )
    {
        return new Vector3(
            MaterializedPositionOffset.x + tilePosition.x * 2,
            0,
            MaterializedPositionOffset.y + tilePosition.y * 2
        );
    }
    public Vector2Int FinalizedSpace(Vector2Int tilePosition) => Position + MaterializedPositionOffset + tilePosition * 2;
    public Vector3 Finalized3DSpace(Vector2Int tilePosition)
    {
        return new Vector3(
            Position.x + MaterializedPositionOffset.x + tilePosition.x * 2, 
            0,
            Position.y + MaterializedPositionOffset.y + tilePosition.y * 2
        );
    }
    public Vector2Int UnitSpace(Vector2Int finalizedTilePosition) {
        Vector2Int t = finalizedTilePosition - (Position + MaterializedPositionOffset);
        return new Vector2Int(t.x/2, t.y/2);
    }
    /// <summary>
    /// Update the bounds based on the tiles in <see cref="cornerTiles"/>
    /// </summary>
    public void UpdateBounds()
    {
        int xMin = int.MaxValue, xMax = int.MinValue,
            yMin = int.MaxValue, yMax = int.MinValue;

        foreach (var corner in cornerTiles)
        {
            if (corner.x < xMin) xMin = corner.x;
            if (corner.x > xMax) xMax = corner.x;
            if (corner.y < yMin) yMin = corner.y;
            if (corner.y > yMax) yMax = corner.y;
        }
        bounds = new RectInt(xMin * 2, yMin * 2, (xMax - xMin) * 2, (yMax - yMin) * 2);
    }

    public void Move(Vector2Int translation) => bounds.position += translation;

    /// <summary>
    /// Returns the vector I need to be moved by in order to lie on the edge of the <paramref name="other"/>
    /// </summary>
    /// <param name="other"></param>
    /// <param name="extraOffset">the extra units to offset by</param>
    /// <returns></returns>
    public Vector2Int SnapToEdgeVector(GeneratingRoom other, int extraOffset = 0)
    {
        Vector2Int difference = Vector2Int.CeilToInt(Bounds.center - other.Bounds.center);
        Vector2 direction = new Vector2(difference.x, difference.y).normalized;
        return SnapToEdgeVector(other, direction, extraOffset);
    }
    /// <summary>
    /// Returns the vector I need to be moved by in order to lie on the edge of the <paramref name="other"/>
    /// </summary>
    /// <param name="other"></param>
    /// <param name="extraOffset">the extra units to offset by</param>
    /// <param name="direction">Which direction relative to <paramref name="other"/> I should be pushed</param>
    /// <returns></returns>
    public Vector2Int SnapToEdgeVector(GeneratingRoom other, Vector2 direction, int extraOffset = 0)
    {
        Vector2Int sizes = (Bounds.size + other.Bounds.size);
        Vector2 offsetExtents = new Vector2(sizes.x / 2 + extraOffset, sizes.y / 2 + extraOffset);

        float offsetDistance = Min(
            offsetExtents.y / Abs(Sin(Atan2(direction.y, direction.x))),
            offsetExtents.x / Abs(Cos(Atan2(direction.y, direction.x)))
        );
        Vector2 realOffset = direction * offsetDistance;

        float 
            xRemainder = (realOffset.x % 2), 
            yRemainder = (realOffset.y % 2);

        return new Vector2Int(
            (int)((xRemainder < 1) ? (realOffset.x - xRemainder) : (realOffset.x - xRemainder + 2)),
            (int)((yRemainder < 1) ? (realOffset.y - yRemainder) : (realOffset.y - yRemainder + 2))
        );
    }

    /// <summary>
    /// Replace all the elements in <see cref="tiles"/> with real instances (they're prefabs before this)
    /// </summary>
    public void Materialize()
    {
        UpdateBounds();
        if (materialized)
            return;
        for (int i = 0; i < roomLayout.tiles.Length; i++)
        {
            roomLayout.tiles[i] = Object.Instantiate(
                roomLayout.tiles[i], 
                new Vector3(roomLayout.tilePositions[i].x * 2, 0, roomLayout.tilePositions[i].y * 2), 
                Quaternion.identity, 
                root.transform
            );
        }
        /*
         * |            |
         * |            |
         * |            |
         * |            |
         * |            |
         * |            *------------
         * |          /       
         * |        /
         * |      v
         * |  
         * |
         * *-----------------------------
         */
        //place room in lower corner of room bounds in order to have consistency between rooms. i.e. no random origin for all rooms
        MaterializedPositionOffset = new Vector2Int(-Position.x, -Position.y);
        root.transform.position = new Vector3(MaterializedPositionOffset.x, 0, MaterializedPositionOffset.y);
        materialized = true;
    }
    /// <summary>
    /// Abort generation, destroy everything related to this object
    /// </summary>
    public void Destroy()
    {
        GameObject finalized = Finalize();
#if UNITY_EDITOR
        Object.DestroyImmediate(finalized, false);
#else
        Object.Destroy(finalized);
#endif
        
    }

    public GameObject Finalize()
    {
        CombineTiles(); //tiles becoming children of the room
        root.transform.position = new Vector3(Position.x, 0, Position.y);
        root.GetComponent<Room>().corners = cornerTiles.Select(tile => FinalizedSpace(tile)).ToArray();
        
        finalized = true;
        return root;
    }

    private void CombineTiles() //TODO: optimize
    {
        MeshFilter[] filters = roomLayout.tiles.Select(t => t.GetComponentInChildren<MeshFilter>()).ToArray();
        MeshCollider[] colliders = roomLayout.tiles.Select(t => t.GetComponentInChildren<MeshCollider>()).ToArray();
        MeshRenderer[] renderers = roomLayout.tiles.Select(t => t.GetComponentInChildren<MeshRenderer>()).ToArray();

        CombineInstance[] colliderCombiner = new CombineInstance[colliders.Length];
        
        HashSet<Material> materials = new HashSet<Material>();
        for (int i = 0; i < renderers.Length; i++)
        {
            colliderCombiner[i] = new CombineInstance() {
                mesh = colliders[i].sharedMesh,
                subMeshIndex = 0,
                transform = colliders[i].transform.localToWorldMatrix
            };

            foreach (Material m in renderers[i].sharedMaterials)
            {
                if (!materials.Contains(m))
                    materials.Add(m);
            }
        }
        Mesh collider = new Mesh();
        collider.CombineMeshes(colliderCombiner, true);
        List<Mesh> submeshes = new List<Mesh>(materials.Count);
        int indexOffset = 0;
        foreach (Material m in materials)
        {
            List<CombineInstance> combiner = new List<CombineInstance>(roomLayout.tiles.Length);
            List<MeshFilter> remainingFilters = new List<MeshFilter>(filters);
            do
            {
                combiner.Clear();
                int vertexCount = 0;
                for (int i = remainingFilters.Count-1; i >= 0; i--)
                {
                    MeshFilter f = filters[i];
                    vertexCount += f.sharedMesh.vertexCount;
                    if (vertexCount >= 65536)
                    {
                        indexOffset++;
                        break;
                    }
                    Renderer r = f.GetComponent<MeshRenderer>();
                    Debug.Assert(r != null, $"Filter:{f} has no renderer, HOW COULD THIS HAPPEN TO ME?");
                    Material[] meshMaterials = r.sharedMaterials;
                    for (int matIndex = 0; matIndex < meshMaterials.Length; matIndex++)
                    {
                        if (meshMaterials[matIndex] != m)
                            continue;
                        combiner.Add(new CombineInstance() {
                            mesh = f.sharedMesh,
                            subMeshIndex = matIndex + meshMaterials.Length * indexOffset,
                            transform = f.transform.localToWorldMatrix
                        });
                    }
                    remainingFilters.RemoveAt(i);
                }
                Mesh mesh = new Mesh();
                mesh.CombineMeshes(combiner.ToArray(), true);
                submeshes.Add(mesh);
            } while (remainingFilters.Count > 0);
        }
        foreach (GameObject go in roomLayout.tiles)
        {
            Object.Destroy(go);
        }
        CombineInstance[] finalCombiner = submeshes.Select(
            m => {
                var instance = new CombineInstance() { mesh = m, subMeshIndex = 0, transform = Matrix4x4.identity };
                return instance;
            }
        ).ToArray();
        Mesh finalMesh = new Mesh();
        finalMesh.CombineMeshes(finalCombiner, false);

        root.AddComponent<MeshFilter>().sharedMesh = finalMesh;
        root.AddComponent<MeshCollider>().sharedMesh = collider;
        var renderer = root.AddComponent<MeshRenderer>();
        renderer.sharedMaterials = materials.ToArray();
        renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.TwoSided;
    }

}
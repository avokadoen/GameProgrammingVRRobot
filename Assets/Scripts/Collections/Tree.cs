﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeNode<T> : IEnumerable<T>
{
    private T data;
    private TreeNode<T> parent;
    private LinkedList<TreeNode<T>> children;
    public T Value => data;
    public int ChildCount => children.Count;
    public int CountTotalTreeNodes()
    {
        int c=0;
        Traverse(this, n => c++);
        return c;
    }
    public TreeNode(T data, TreeNode<T> parent = null)
    {
        this.data = data;
        this.parent = parent;
        children = new LinkedList<TreeNode<T>>();
    }

   /// <summary>
   /// 
   /// </summary>
   /// <param name="newRoot"></param>
   /// <returns>New Root</returns>
    public static TreeNode<T> Rotate(TreeNode<T> newRoot)
    {
        if (newRoot.parent == null)
            return newRoot;
        TreeNode<T> current = newRoot.parent;
        newRoot.parent = null;
        if (current.parent != null)
        { Rotate(current); }
        if (current.children.Remove(newRoot))
        {
            current.parent = null;
            newRoot.AddChildNode(current);
        }
        return newRoot;
    }
    //private void Swap(TreeNode<T> newParent)
    //{
    //    if (parent != null)
    //    {
    //        parent.children.Remove(this);
    //        parent.AddChildNode(newParent);
    //    }
    //    children.Remove(newParent);
    //    newParent.AddChildNode(this);
    //}

    public void Attach(TreeNode<T> subtree, TreeNode<T> attachTo)
    { attachTo.AddChildNode(subtree); }
    private void AddChildNode(TreeNode<T> child)
    {
        children.AddFirst(child);
        Debug.Assert(child.parent == null, "Oh shieet, tried to add a child node that already is parented to someone else, see Rotate(...) to fix");
        child.parent = this;
    }
    public void AddChild(T data)
    { children.AddFirst(new TreeNode<T>(data, this)); }

    public TreeNode<T> GetNode(T data)
    {
        TreeNode<T> node = null;
        Traverse(this, n => { if (n.data.Equals(data)) { node = n; return; } });
        return node;
    }
    public TreeNode<T> GetChild(int i)
    {
        i++;
        foreach (var n in children)
        {
            if (--i == 0)
                return n;
        }
        return null;
    }

    public IEnumerator<TreeNode<T>> Nodes => children.GetEnumerator();

    public IEnumerator<T> GetEnumerator()
    {
        yield return data;
        foreach (var childNode in children)
            foreach (var child in childNode)
                yield return child;
    }
    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();



    /// <summary>
    /// Traverse the tree in a depth first manner,
    /// <paramref name="node"/> is the first to be touched by visitor, after that visitor touches children from the outmost nodes (leaves), and inwards -... what the fuck am i writing this sounds really really bad...?
    /// </summary>
    /// <param name="node"></param>
    /// <param name="visitor"></param>
    public void Traverse(TreeNode<T> node, System.Action<TreeNode<T>> visitor)
    {
        visitor(node);
        foreach (TreeNode<T> child in node.children)
            Traverse(child, visitor);
    }
    /// <summary>
    /// Traverse the tree in a depth first manner,
    /// NOTE: calling node will be the first node traversed, i.e. not the children as you might expect!
    /// calling node is the first to be touched by visitor, after that visitor touches children from the outmost nodes (leaves), and inwards -... what the fuck am i writing this sounds really really bad...?
    /// </summary>
    /// <param name="visitor"></param>
    public void Traverse( System.Action<TreeNode<T>> visitor ) => Traverse(this, visitor);
    /// <summary>
    /// Traverse the tree in a depth first manner till <paramref name="depth"/>
    /// where <paramref name="node"/> is at depth=0, child of <paramref name="node"/> is 1, child of child is at 2. etc...
    /// </summary>
    /// <param name="node"></param>
    /// <param name="visitor"></param>
    public void Traverse(TreeNode<T> node, System.Action<TreeNode<T>> visitor, int depth)
    {
        visitor(node);
        if (depth <= 0) return;
        foreach (TreeNode<T> child in node.children)
            Traverse(child, visitor, depth-1);
    }

    /// <summary>
    /// Traverse the tree in a depth first manner till <paramref name="depth"/>
    /// where the calling node is at depth=0, child of calling node is 1, child of child is at 2. etc...
    /// </summary>
    /// <param name="visitor"></param>
    public void Traverse( System.Action<TreeNode<T>> visitor, int depth ) => Traverse(this, visitor, depth);
}

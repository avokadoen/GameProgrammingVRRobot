﻿using UnityEngine;
using UnityEditor;

namespace EditorUtils
{
    //DIRECTLY COPIED FROM DECOMPILED UNITY CODE:
    //https://github.com/jamesjlinden/unity-decompiled
    public class SavedFloat
    {
        private float m_Value;
        private string m_Name;

        public float Value
        {
            get
            {
                return m_Value;
            }
            set
            {
                if (m_Value == value)
                    return;
                m_Value = value;
                EditorPrefs.SetFloat(m_Name, value);
            }
        }

        public SavedFloat(string name, float value)
        {
            m_Name = name;
            m_Value = EditorPrefs.GetFloat(name, value);
        }

        public static implicit operator float(SavedFloat s) => s.Value;
    }

    public class SavedBool
    {
        private bool m_Value;
        private string m_Name;

        public bool Value
        {
            get
            {
                return m_Value;
            }
            set
            {
                if (m_Value == value)
                    return;
                m_Value = value;
                EditorPrefs.SetBool(m_Name, value);
            }
        }

        public SavedBool(string name, bool value)
        {
            m_Name = name;
            m_Value = EditorPrefs.GetBool(name, value);
        }

        public static implicit operator bool(SavedBool s) => s.Value;
    }

    public class SavedInt
    {
        private int m_Value;
        private string m_Name;

        public int Value
        {
            get
            {
                return m_Value;
            }
            set
            {
                if (m_Value == value)
                    return;
                m_Value = value;
                EditorPrefs.SetInt(m_Name, value);
            }
        }

        public SavedInt(string name, int value)
        {
            m_Name = name;
            m_Value = EditorPrefs.GetInt(name, value);
        }

        public static implicit operator int(SavedInt s) => s.Value;
    }

    public class SavedString
    {
        private string m_Value;
        private string m_Name;

        public string Value
        {
            get
            {
                return m_Value;
            }
            set
            {
                if (m_Value == value)
                    return;
                m_Value = value;
                EditorPrefs.SetString(m_Name, value);
            }
        }

        public SavedString(string name, string value)
        {
            m_Name = name;
            m_Value = EditorPrefs.GetString(name, value);
        }

        public static implicit operator string(SavedString s) => s.Value;
    }
}
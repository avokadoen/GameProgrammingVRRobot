﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HubTeleporter : MonoBehaviour {

    public GameObject teleportIndicator;
    public GameManager gameManager;
    public WarTableNode currentChosenNode;
    public bool activated;

	// Use this for initialization
	void Start () {
		if(teleportIndicator == null)
        {
            Debug.LogError("TeleportIndicator not loaded correctly");
            teleportIndicator = transform.Find("TeleportIndicator").gameObject;
        }
        teleportIndicator.GetComponent<Renderer>().material.SetColor("_Color", Color.red);

        gameManager = FindObjectOfType<GameManager>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ActivateTeleporter(WarTableNode node)
    {
        currentChosenNode = node;
        activated = true;
        teleportIndicator.GetComponent<Renderer>().material.SetColor("_Color", Color.green);
    }

    public void DeActivateTeleporter()
    {
        currentChosenNode = null;
        activated = false;
        teleportIndicator.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && activated)
        {
            DeActivateTeleporter();
            gameManager.LoadNewLevel();
        }
    }
}

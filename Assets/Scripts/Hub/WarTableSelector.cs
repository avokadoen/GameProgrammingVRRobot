﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/* Known issues with this:
 * Can collide with objects and be sent away, needs to be collected by onCollisionEnter and sent back to a valid position
 * just like in OnDrop.
 * 
 */
public class WarTableSelector : PhysicalObject {

    private Vector3 initialPosition;
    public WarTableNode lastNode;
    public WarTableNode currentNode;
    public HubTeleporter hubTeleporter;
    public GameManager gameManager;
    private const int WarTableNodeLayerMask = 8192;


    void Start () {
        initialPosition = transform.position;
        if(hubTeleporter == null)
        {
            Debug.LogError("HubTeleporter not initalized correctly in prefab");
            hubTeleporter = GameObject.Find("Teleporter").GetComponent<HubTeleporter>();
        }
        gameManager = FindObjectOfType<GameManager>();
    }
	
    public override void OnDrop(Transform droppedByHand, Vector3 velocity)
    {
        base.OnDrop(droppedByHand, velocity);

        Debug.Log("Dropped selector");
        float closestDistance = Mathf.Infinity;
        WarTableNode bestNode = null;
        Collider[] nodes = Physics.OverlapSphere(transform.position, 0.5f, WarTableNodeLayerMask);
        foreach (Collider node in nodes)
        {
            Debug.Log("Hit Collider" + node.name);
            float distance = (transform.position - node.transform.position).sqrMagnitude;
            WarTableNode warTableNode = node.GetComponent<WarTableNode>();
            if (warTableNode.activated && distance < closestDistance)
            {
                closestDistance = distance;
                bestNode = warTableNode;
            }
        }

        if(bestNode == null)
        {
            if (lastNode != null)
            {
                transform.rotation = Quaternion.identity;
                transform.position = lastNode.transform.position;
            }
            else
            {
                transform.rotation = Quaternion.identity;
                transform.position = initialPosition;
            }
        }
        else
        {
            currentNode = bestNode;
            transform.rotation = Quaternion.identity;
            transform.position = currentNode.transform.position;
            currentNode.SelectNode();
            gameManager.currentLevel = bestNode.levelNode;
            hubTeleporter.ActivateTeleporter(currentNode);
            FindObjectOfType<WorldMapGenerator>().UpdateTableState();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerVulnerablePart : MonoBehaviour {

    [SerializeField] private PlayerState playerState;

    private void Start()
    {
        if(playerState == null)
        {
            Debug.LogError("body part missing playerState");
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        playerState.OnBodyPartCollisionEnter(collision);
    }

    private void OnTriggerEnter(Collider other)
    {
        playerState.OnBodyPartTriggerEnter(other);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicalStorage : MonoBehaviour {

    [Tooltip("should be magazine and weapon")]
    public LayerMask StoreableLayers;

    public Transform playerTransform;

    List<GameObject> itemsToStore = new List<GameObject>();

    private void OnTriggerEnter(Collider other)
    {
        if (StoreableLayers != (StoreableLayers | (1 << other.gameObject.layer)))
        {
            return;
        }
        if (!itemsToStore.Contains(other.gameObject))
        {
            other.GetComponent<PhysicalObject>().onPlayerDrop.AddListener(AttachItem);
            itemsToStore.Add(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (StoreableLayers != (StoreableLayers | (1 << other.gameObject.layer)))
        {
            return;
        }
        if (itemsToStore.Contains(other.gameObject))
        {
            other.GetComponent<PhysicalObject>().onPlayerDrop.RemoveListener(AttachItem);
            itemsToStore.Remove(other.gameObject);
        }
    }


    void AttachItem(Transform itemToStore)
    {
        PhysicalObject physicalItemToStore = itemToStore.gameObject.GetComponent<PhysicalObject>();

        if (itemsToStore.Contains(itemToStore.gameObject))
        {
            physicalItemToStore.onPlayerDrop.RemoveListener(AttachItem);
            itemsToStore.Remove(itemToStore.gameObject);
        }

        physicalItemToStore.body.isKinematic = true;
        physicalItemToStore.body.velocity = Vector3.zero;

        itemToStore.parent = transform;
        //itemToStore.position = playerTransform.position - itemToStore.position;
    }


}

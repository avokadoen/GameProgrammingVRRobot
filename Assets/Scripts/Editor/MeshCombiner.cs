﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Collections.Generic;

public static class MeshCombiner
{
    static MeshFilter[] filters;

    [MenuItem("Custom/Combine Mesh", validate = true)]
    static bool ValidateCombinable()
    {
        if (Selection.activeGameObject == null)
            return false;
        filters = Selection.activeGameObject.GetComponentsInChildren<MeshFilter>();
        return filters.Length > 0;
    }
    [MenuItem("Custom/Combine Mesh")]
    public static void CombineSelected()
    {
        Undo.RegisterCompleteObjectUndo(Selection.activeGameObject, "Combining meshes");
        MeshRenderer[] renderers = Selection.activeGameObject.GetComponentsInChildren<MeshRenderer>();
        HashSet<Material> materials = new HashSet<Material>();
        foreach (MeshRenderer r in renderers)
        {
            foreach (Material m in r.sharedMaterials)
            {
                if (!materials.Contains(m))
                    materials.Add(m);
            }
        }
        List<Mesh> submeshes = new List<Mesh>(materials.Count);
        foreach (Material m in materials)
        {
            List<CombineInstance> combiner = new List<CombineInstance>(filters.Length);
            foreach (MeshFilter f in filters)
            {
                Renderer r = f.GetComponent<MeshRenderer>();
                Debug.Assert(r != null, $"Filter:{f} has no renderer, HOW COULD THIS HAPPEN TO ME?");
                Material[] meshMaterials = r.sharedMaterials;
                for (int matIndex = 0; matIndex < meshMaterials.Length; matIndex++)
                {
                    if (meshMaterials[matIndex] != m)
                        continue;
                    combiner.Add(new CombineInstance()
                    {
                        mesh = f.sharedMesh,
                        subMeshIndex = matIndex,
                        transform = f.transform.localToWorldMatrix
                    });
                }
            }
            Mesh mesh = new Mesh();
            mesh.CombineMeshes(combiner.ToArray(), true);
            submeshes.Add(mesh);
        }
        foreach (MeshFilter filter in filters)
        {
            Undo.DestroyObjectImmediate(filter.gameObject);
        }
        CombineInstance[] finalCombiner = submeshes.Select(
            m => new CombineInstance() { mesh = m, subMeshIndex = 0, transform = Matrix4x4.identity }
        ).ToArray();
        Mesh finalMesh = new Mesh();
        finalMesh.CombineMeshes(finalCombiner, false);

        Selection.activeGameObject.AddComponent<MeshFilter>().sharedMesh = finalMesh;
        Selection.activeGameObject.AddComponent<MeshRenderer>().sharedMaterials = materials.ToArray();
        Selection.activeGameObject.AddComponent<MeshCollider>().sharedMesh = finalMesh;

    }
}
#endif
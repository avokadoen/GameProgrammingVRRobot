﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using static UnityEngine.Mathf;
public class RoomGenerator : MonoBehaviour
{
    public RoomType[] roomsToGenerate;

    public string seed = "lmao";


    public GeneratingRoom GenerateRoom( System.Random random, RoomType roomType, Vector2Int roomOrigin )
    {
        GeneratingRoom room = new GeneratingRoom($"{roomType.name} Room - {seed}")
        {
            roomType = roomType,
            tileSet = roomType.roomSets.GetRandom(random) //select which variant of the room we want
        };
        
        int size = random.Next(roomType.sizeLimits.x, roomType.sizeLimits.y);
        Vector2Int workSpace = new Vector2Int(size, size);

        Vector2Int p = new Vector2Int(workSpace.x / 2, workSpace.y/2) + new Vector2Int(
            random.NextInclusive(-workSpace.x/2, workSpace.x/2), 
            random.NextInclusive(-workSpace.y/2, workSpace.y/2)
        );
        
        for (int cluster = 0; cluster < roomType.roomClusters; cluster++, p = room.wallTiles.GetRandom(random)) //pick from openset instead
        {
            Vector2Int q = new Vector2Int(random.Next(0, workSpace.x), random.Next(0, workSpace.y));
            Vector2Int start = new Vector2Int(Min(p.x, q.x), Min(p.y, q.y));
            Vector2Int end = new Vector2Int(Max(p.x, q.x), Max(p.y, q.y));
            //Debug.Log($"New room start: [x:{p.x}, y:{p.y}]");

            
            //Debug.Log($"New room rect: [x:{p.x}, y:{p.y}, xMax:{q.x}, yMax:{q.y}]");
            for (int y = start.y; y <= end.y; y++)
            {
                bool yExtremum = (y == start.y || y == end.y);
                for (int x = start.x; x <= end.x; x++)
                {
                    Vector2Int tile = new Vector2Int(x, y);
                    if (yExtremum || x == start.x || x == end.x)
                        room.wallTiles.Add(tile);
                    else if (room.wallTiles.Contains(tile))
                        room.wallTiles.Remove(tile);
                    room.allTiles.Add(tile);
                }
            }
        }

        room.roomLayout = new GeneratingRoom.RoomLayout(room.allTiles.Count); //Allocate space for all the gameobjects needed
        RoomType.RoomSet roomSet = room.tileSet;
        int i = 0;
        foreach (Vector2Int t in room.allTiles)
        {
            TileType tileTypeMask = TileType.FLOOR;

            if (room.wallTiles.Contains(t))
            {
                Tile tile = new Tile(t);
                if (!room.allTiles.Contains(tile.Right)) tileTypeMask |= TileType.WALL_EAST;
                if (!room.allTiles.Contains(tile.Up))    tileTypeMask |= TileType.WALL_NORTH;
                if (!room.allTiles.Contains(tile.Left))  tileTypeMask |= TileType.WALL_WEST;
                if (!room.allTiles.Contains(tile.Down))  tileTypeMask |= TileType.WALL_SOUTH;
            }

            GameObject targetTile;

            switch (tileTypeMask)
            {
                case TileType.FLOOR: 
                    targetTile = roomSet.Floors.GetRandom(random);
                    room.floorTiles.Add(t);
                    room.wallTiles.Remove(t);
                    break;
                case TileType.WALL_SOUTH:
                    targetTile = roomSet.Walls_South.GetRandom(random);
                    room.southWalls.Add(t);
                    break;
                case TileType.WALL_WEST:
                    targetTile = roomSet.Walls_West.GetRandom(random);
                    room.westWalls.Add(t);
                    break;
                case TileType.CORNER_SOUTHWEST:
                    targetTile = roomSet.Corners_SouthWest.GetRandom(random);
                    room.cornerTiles.Add(t);
                    break;
                case TileType.WALL_NORTH:
                    targetTile = roomSet.Walls_North.GetRandom(random);
                    room.northWalls.Add(t);
                    break;
                case TileType.CORRIDOR_EASTWEST:
                    targetTile = roomSet.Corridors_EastWest.GetRandom(random);
                    break;
                case TileType.CORNER_NORTHWEST:
                    targetTile = roomSet.Corners_NorthWest.GetRandom(random);
                    room.cornerTiles.Add(t);
                    break;
                case TileType.WALL_EAST:
                    targetTile = roomSet.Walls_East.GetRandom(random);
                    room.eastWalls.Add(t);
                    break;
                case TileType.CORNER_SOUTHEAST:
                    targetTile = roomSet.Corners_SouthEast.GetRandom(random);
                    room.cornerTiles.Add(t);
                    break;
                case TileType.CORRIDOR_NORTHSOUTH:
                    targetTile = roomSet.Corridors_NorthSouth.GetRandom(random);
                    break;
                case TileType.CORNER_NORTHEAST:
                    targetTile = roomSet.Corners_NorthEast.GetRandom(random);
                    room.cornerTiles.Add(t);
                    break;
                default:
                    //TODO resolve this, either by making the tile a valid tile: corridor end caps
                    Debug.LogWarning($"Tile: {t} unable to assign tile type, UNUSED combination: {tileTypeMask}, Added as cornerTile to make sure the bounds are set correctly");
                    targetTile = roomSet.Floors.GetRandom(random);
                    room.cornerTiles.Add(t); //Not _really_ a corner, but add it anyways to make sure bounds are correct
                    break;
            }

            //load the array with prototypes for now...
            room.roomLayout.tiles[i] = targetTile;
            room.roomLayout.tilePositions[i] = t;
            i++;
        }
        room.Materialize();
        return room;
    }
}


//  RIGHT   UP      LEFT    DOWN
//  1       1       1       1
[Flags]
enum TileType : byte //TODO use these as indices for tileset-lookup
{
    FLOOR = 0,
    WALL_SOUTH = 1,
    WALL_WEST = 2,
    CORNER_SOUTHWEST = 3,
    WALL_NORTH = 4,
    CORRIDOR_EASTWEST = 5,
    CORNER_NORTHWEST = 6,
    //UNUSED_DOWN_LEFT_UP = 7,
    WALL_EAST = 8,
    CORNER_SOUTHEAST = 9,
    CORRIDOR_NORTHSOUTH = 10,
    //UNUSED_DOWN_LEFT_RIGHT = 11,
    CORNER_NORTHEAST = 12,
    //UNUSED_DOWN_UP_RIGHT = 13,
    //UNUSED_UP_LEFT_RIGHT = 14,
    //WALLED_IN = 15
}

public struct Tile
{
    public Tile Right => new Tile(position + Vector2Int.right);
    public Tile Up    => new Tile(position + Vector2Int.up);
    public Tile Left  => new Tile(position + Vector2Int.left);
    public Tile Down  => new Tile(position + Vector2Int.down);
    public float DistanceToOrigin => position.magnitude;

    public Vector2Int position;
    public Tile(Vector2Int pos) { position = pos; }
    //NOTE: is this bad practice? idk
    public static implicit operator Vector2Int(Tile tile) => tile.position;
    public static explicit operator Tile(Vector2Int pos) => new Tile(pos);
}
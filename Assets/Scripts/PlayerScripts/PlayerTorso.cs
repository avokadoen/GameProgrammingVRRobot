﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTorso : MonoBehaviour {

    [SerializeField]
    private Transform head;

    [Tooltip("Relative to head")]
    [SerializeField]
    private Vector3 TorsoOffset;


    // Use this for initialization
    void Start () {
        if (head == null)
            Debug.LogError("head is null in PlayerTorso");
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 offsetZ = head.forward * TorsoOffset.z;
        Vector3 offsetX = head.forward * TorsoOffset.x;
        Vector3 currentOffset = new Vector3(offsetZ.x + offsetX.x, TorsoOffset.y, offsetZ.z + offsetX.z);
        transform.position = head.position + currentOffset;
        transform.rotation = Quaternion.Euler(0, head.rotation.eulerAngles.y, 0);
    }
}

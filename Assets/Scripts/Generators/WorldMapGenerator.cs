﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

/*
public struct LevelNode  // Should probably be it's own scriptable object
{
    public bool visibleType;
    public Vector2Int  position;
    public Vector2Int? parent;
    public Vector2Int? rightChild;
    public Vector2Int? leftChild;
    public Vector2Int? forwardChild;
    public LevelType   levelType;
}*/

public class WorldMapGenerator : MonoBehaviour {

    [SerializeField] private int maxDeviation;
    [SerializeField] private int stagesToGenerate;
    [SerializeField] private int visibleOdds;
    [SerializeField] private int leftOdds;
    [SerializeField] private int forwardOdds;
    [SerializeField] private int rightOdds;
    [SerializeField] public string testSeed = "lmao";

    // For debugging
    public GameObject lineRenderer;
    public GameObject nodeObject;
    
    public LevelType[] levelTypes;
    public LevelNode[,] nodeMap;
    public WarTableNode[,] warTableNodeMap;
    public WarTableSelector warTableSelector;
    public GameManager gameManager;
    public System.Random random;

    private int nodeFields => maxDeviation * 2;

    private void GenerateWorldMap(System.Random random)
    {
        nodeMap = new LevelNode[nodeFields + 1, stagesToGenerate];
        List<List<LevelType>> accessibleLevelTypes = new List<List<LevelType>>();
        int[] currentStageRating;
        currentStageRating = new int[100];
        LevelNode currentNode;
        Vector2Int endNodePosition = new Vector2Int(maxDeviation, stagesToGenerate - 1);

        for (int stagesToGen = 0; stagesToGen < stagesToGenerate; stagesToGen++)
        {
            accessibleLevelTypes.Add(new List<LevelType>());
            AddLegalLevelTypes(ref accessibleLevelTypes, stagesToGen);
            currentStageRating[stagesToGen] = GetStageTotalRating(accessibleLevelTypes, stagesToGen);
        }


        for (int stage = 0; stage < stagesToGenerate; stage++)
        {
            //AddLegalLevelTypes(ref accessibleLevelTypes, stage);
            //int totalStageRating = GetStageTotalRating(accessibleLevelTypes, stage);
            if (stage == 0)
            {   // Create root node
                Vector2Int nodePosition = new Vector2Int(maxDeviation, stage);
                nodeMap[nodePosition.x, nodePosition.y] 
                    = CreateLevelNode(random, currentStageRating[nodePosition.y], nodePosition, null, accessibleLevelTypes);
            }

            while((currentNode = GetNextNodeOnStage(stage, endNodePosition)) != null)
            {
                while (!currentNode.HasChilds() && currentNode.position != endNodePosition)
                {
                    Vector2Int left     = new Vector2Int(currentNode.position.x - 1, currentNode.position.y);
                    Vector2Int right    = new Vector2Int(currentNode.position.x + 1, currentNode.position.y);
                    Vector2Int forward  = new Vector2Int(currentNode.position.x,     currentNode.position.y + 1);
                    if(stage == endNodePosition.y)
                    {
                        if(currentNode.position.x < endNodePosition.x)
                        {
                            if (nodeMap[right.x, right.y] != null && !nodeMap[right.x, right.y].IsAlreadyRelation(currentNode.position))
                            {

                            }
                            else
                            {
                                nodeMap[right.x, right.y]
                                    = CreateLevelNode(random, currentStageRating[right.y], right, currentNode.position, accessibleLevelTypes);
                            }
                            currentNode.rightChild = right;
                        }
                        else if(currentNode.position.x > endNodePosition.x)
                        {
                            if (nodeMap[left.x, left.y] != null && !nodeMap[left.x, left.y].IsAlreadyRelation(currentNode.position))
                            {

                            }
                            else
                            {
                                nodeMap[left.x, left.y]
                                    = CreateLevelNode(random, currentStageRating[left.y], left, currentNode.position, accessibleLevelTypes);
                            }
                            currentNode.leftChild = left;
                        }
                    }
                    else
                    { 
                        if (currentNode.parent != left && currentNode.position.x != 0 && (0 == random.Next() % leftOdds))
                        {
                            if(nodeMap[left.x, left.y] != null && !nodeMap[left.x, left.y].IsAlreadyRelation(currentNode.position))
                            {

                            }
                            else
                            {
                                nodeMap[left.x, left.y] 
                                    = CreateLevelNode(random, currentStageRating[left.y], left, currentNode.position, accessibleLevelTypes);
                            }
                            currentNode.leftChild = left;
                        }

                        if (currentNode.parent != right && currentNode.position.x != (nodeFields) && (0 == random.Next() % rightOdds))
                        {
                            if (nodeMap[right.x, right.y] != null && !nodeMap[right.x, right.y].IsAlreadyRelation(currentNode.position))
                            {

                            }
                            else
                            {
                                nodeMap[right.x, right.y]
                                    = CreateLevelNode(random, currentStageRating[right.y], right, currentNode.position, accessibleLevelTypes);
                            }
                            currentNode.rightChild = right;
                        }

                        if (currentNode.position.y != (stagesToGenerate - 1) && (0 == random.Next() % forwardOdds))
                        {
                            if (nodeMap[forward.x, forward.y] != null)
                            {

                            }
                            else
                            {
                                nodeMap[forward.x, forward.y]
                                    = CreateLevelNode(random, currentStageRating[forward.y], forward, currentNode.position, accessibleLevelTypes);
                            }
                            currentNode.forwardChild = forward;
                        }
                    }
                }
            }
        }
    }

    private LevelNode CreateLevelNode(System.Random random, int totalStageRating, Vector2Int mapPosition, Vector2Int? parent, List<List<LevelType>> accessibleLevelTypes)
    {
        LevelNode tempNode = ScriptableObject.CreateInstance<LevelNode>();

        tempNode.parent         = parent;
        tempNode.position.y     = mapPosition.y;
        tempNode.position.x     = mapPosition.x;
        tempNode.visibleType    = (0 == random.Next() % visibleOdds);
        tempNode.levelType      = PickRandomLevelType(random, accessibleLevelTypes, totalStageRating, mapPosition.y);

        return tempNode;
    }

    private void AddLegalLevelTypes(ref List<List<LevelType>> accessibleLevelTypes, int currentStage)
    {
        foreach (LevelType levelType in levelTypes)
        {
            if(levelType.spawnFromStage <= currentStage)
            {
                accessibleLevelTypes[currentStage].Add(levelType);
            }
        }
    }

    private LevelType PickRandomLevelType(System.Random random, List<List<LevelType>> accessibleLevelTypes, int totalStageRating, int currentStage)
    {
        //TODO: This function ends up failing at picking a level at times, probably because of spawnrating
        int spawnNumber = random.Next(totalStageRating);
        Debug.Log("totalStageRating:" + totalStageRating);
        Debug.Log("accessibleLevelTypes:" + accessibleLevelTypes[currentStage].Count);
        Debug.Log("spawnNumber:" + spawnNumber);
        Debug.Log("current stage:" + currentStage);

        foreach (LevelType levelType in accessibleLevelTypes[currentStage])
        {
            if(levelType.spawnRating[currentStage] >= spawnNumber)
            {
                return levelType;
            }
            else
            {
                spawnNumber -= levelType.spawnRating[currentStage];
            }
        }
        Debug.Log("Something went wrong");
        LevelType tempType = new LevelType();
        return tempType;
    }

    private int GetStageTotalRating(List<List<LevelType>> accessibleLevelTypes, int currentStage)
    {
        int tempRating = 0;
        foreach (LevelType levelType in accessibleLevelTypes[currentStage])
        {
            tempRating += levelType.spawnRating[currentStage];
        }
        return tempRating;
    }

    private LevelNode GetNextNodeOnStage(int currentStage, Vector2Int endNode)
    {
        for(int i = 0; i <= nodeFields; i++)
        {
            Vector2Int checkPos = new Vector2Int(i, currentStage);
            if(nodeMap[i, currentStage] != null && checkPos != endNode)
            {
                if(!nodeMap[i, currentStage].HasChilds())
                {
                    return nodeMap[i, currentStage];
                }
            }
        }
        return null;
    }

    private void InitWorldMapVisualization()
    {
        // TODO: Maybe get WarTable first from RequireComponent, then use that's transform position as initial.
        GameObject warTable = GameObject.FindGameObjectWithTag("WarTable");
        BoxCollider warTableBounds = warTable.GetComponent<BoxCollider>();  // TODO: Maybe better way to do this, with bounds or something?
        float totalXRange = warTableBounds.size.x / 2;
        float totalZRange = warTableBounds.size.z; //*2 for actual
        float lineOffset = 0.05f; // TODO: Work around this to make it look better
        Vector2 offset = new Vector2(totalXRange / maxDeviation, totalZRange / stagesToGenerate);
        Vector3 rootCoordinates =
                new Vector3(warTable.transform.position.x - (warTableBounds.size.x / 2),
                            warTable.transform.position.y + warTableBounds.size.y,
                            warTable.transform.position.z - (warTableBounds.size.z / 2) + offset.y / 2);
        foreach (LevelNode levelNode in nodeMap)
        {
            if(levelNode != null)
            {
                Vector3 coordinates =
                new Vector3(rootCoordinates.x + (levelNode.position.x * offset.x),
                            rootCoordinates.y,
                            rootCoordinates.z + (levelNode.position.y * offset.y));
                GameObject warTableNode = Instantiate(levelNode.levelType.warTablePrefab, coordinates, Quaternion.identity);
                warTableNode.transform.parent = warTable.transform;
                WarTableNode currentWarTableNode = warTableNode.GetComponent<WarTableNode>();
                warTableNodeMap[levelNode.position.x, levelNode.position.y] = currentWarTableNode;
                currentWarTableNode.levelNode = levelNode;
                coordinates.y += lineOffset;
                if (levelNode.rightChild != null)
                {
                    GameObject linePath = Instantiate(lineRenderer);
                    LineRenderer line = linePath.GetComponent<LineRenderer>();
                    linePath.GetComponent<Renderer>().material.SetInt("_Direction", 0);

                    Vector3[] nodePositions = new Vector3[2];
                    nodePositions[0] = coordinates;
                    nodePositions[1] = new Vector3(rootCoordinates.x + ((float)levelNode.rightChild?.x * offset.x),
                            rootCoordinates.y + lineOffset, // Just to make it not stick to the table
                            rootCoordinates.z + ((float)levelNode.rightChild?.y * offset.y));
                    line.SetPositions(nodePositions);
                    line.transform.parent = warTableNode.transform;
                    currentWarTableNode.lineRenderers.Add(line);
                }
                if(levelNode.leftChild != null)
                {
                    GameObject linePath = Instantiate(lineRenderer);
                    LineRenderer line = linePath.GetComponent<LineRenderer>();
                    linePath.GetComponent<Renderer>().material.SetInt("_Direction", 1);

                    Vector3[] nodePositions = new Vector3[2];
                    nodePositions[0] = coordinates;
                    nodePositions[1] = new Vector3(rootCoordinates.x + ((float)levelNode.leftChild?.x * offset.x),
                            rootCoordinates.y + lineOffset, // Just to make it not stick to the table
                            rootCoordinates.z + ((float)levelNode.leftChild?.y * offset.y));
                    line.SetPositions(nodePositions);
                    line.transform.parent = warTableNode.transform;
                    currentWarTableNode.lineRenderers.Add(line);
                }
                if(levelNode.forwardChild != null)
                {
                    GameObject linePath = Instantiate(lineRenderer);
                    LineRenderer line = linePath.GetComponent<LineRenderer>();
                    linePath.GetComponent<Renderer>().material.SetInt("_Direction", 2);

                    Vector3[] nodePositions = new Vector3[2];
                    nodePositions[0] = coordinates;
                    nodePositions[1] = new Vector3(rootCoordinates.x + ((float)levelNode.forwardChild?.x * offset.x),
                            rootCoordinates.y + lineOffset, // Just to make it not stick to the table
                            rootCoordinates.z + ((float)levelNode.forwardChild?.y * offset.y));
                    line.SetPositions(nodePositions);
                    line.transform.parent = warTableNode.transform;
                    currentWarTableNode.lineRenderers.Add(line);
                }
            }
        }
    }

    public void InitTableState()
    {
        LevelNode rootNode = nodeMap[maxDeviation, 0];
        WarTableNode rootNodeObject = warTableNodeMap[maxDeviation, 0];
        rootNodeObject.ActivateNode();
        //warTableSelector.currentNode = rootNodeObject;
        //warTableSelector.transform.rotation = Quaternion.identity;
        //warTableSelector.transform.position = warTableSelector.currentNode.transform.position;
        //warTableSelector.hubTeleporter.ActivateTeleporter(rootNodeObject);
        //gameManager.currentLevel = rootNode;
        UpdateTableState();
    }

    public void UpdateTableState()
    {
        foreach(WarTableNode node in warTableNodeMap)
        {
            if(node != null)
            {
                // TODO: Debug this, has some issues
                if (node.playedThrough == true && node == warTableSelector.currentNode)
                {
                    node.PlayedThroughNode();
                    ActivateChildNodes(node.levelNode);
                    node.UpdateOpenPaths();
                }
                else if (node.playedThrough == true)
                {
                    node.PlayedThroughNode();
                    node.UpdateClosePaths();
                }
                else if (node == warTableSelector.currentNode)
                {
                    node.SelectNode();
                    node.UpdateClosePaths();
                }
                else if (node.activated == true)
                {
                    node.ActivateNode();
                    node.UpdateClosePaths();
                }
                else
                {
                    node.DeActivateNode();
                    node.UpdateClosePaths();
                }
            }
        }
    }

    public void UpdateCompletedNodes(List<LevelNode> completedNodes)
    {
        foreach(LevelNode node in completedNodes)
        {
            warTableNodeMap[node.position.x, node.position.y].playedThrough = true;
        }
        UpdateTableState();
    }

    public WarTableNode GetWarTableNode(Vector2Int position)
    {
        return warTableNodeMap[position.x, position.y];
    }

    private void ActivateChildNodes(LevelNode node)
    {
        if(node.leftChild != null)
        {
            warTableNodeMap[(int)node.leftChild?.x, (int)node.leftChild?.y].GetComponent<WarTableNode>().ActivateNode();
        }
        if (node.rightChild != null)
        {
            warTableNodeMap[(int)node.rightChild?.x, (int)node.rightChild?.y].GetComponent<WarTableNode>().ActivateNode();
        }
        if (node.forwardChild != null)
        {
            warTableNodeMap[(int)node.forwardChild?.x, (int)node.forwardChild?.y].GetComponent<WarTableNode>().ActivateNode();
        }
    }

    // TODO: Remove this into a function call from the GameManager?
    void Start () {
        if (warTableSelector == null)
        {
            warTableSelector = GameObject.Find("WarTableSelector").GetComponent<WarTableSelector>();
            if (warTableSelector == null)
            {
                Debug.LogError("WarTableSelector not initialized in scene properly");
                return;
            }
        }
        warTableNodeMap = new WarTableNode[nodeFields + 1, stagesToGenerate];
        gameManager = FindObjectOfType<GameManager>();
        if (!gameManager.warTableInstantiated)
        {
            if (gameManager.rand == null)
                gameManager.InstantiateRandom();
            GenerateWorldMap(gameManager.rand);
            gameManager.nodeMapCopy = nodeMap;
            gameManager.warTableInstantiated = true;
        }
        else
        {
            nodeMap = gameManager.nodeMapCopy;
        }
        // TODO: Make sure that the wartable takes in the maps that have been completed by the player, and accordingly updates the wartable
        InitWorldMapVisualization();
        InitTableState();
    }
	
}


//                  NODE-FIELDS                  //
//  0  //  1  //  2  //  3  //  4  //  5  //  6  // Stage
// --- // --- // --- // --- // --- // --- // --- //   0
// --- // --- // --- // --- // --- // --- // --- //   1
// --- // --- // --- // --- // --- // --- // --- //   2
// --- // --- // --- // --- // --- // --- // --- //   3
// --- // --- // --- // --- // --- // --- // --- //   4
// --- // --- // --- // --- // --- // --- // --- //   5
// --- // --- // --- // --- // --- // --- // --- //   6
// --- // --- // --- // --- // --- // --- // --- //   7
// --- // --- // --- // --- // --- // --- // --- //   8
// --- // --- // --- // --- // --- // --- // --- //   9


/* Variables that kinda work:
 * maxDeviation = 2
 * stagesToGenerate = 8
 * leftOdds = 4
 * forwardOdds = 3
 * rightOdds = 4
 */
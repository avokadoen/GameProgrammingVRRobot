﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using Random = System.Random;

[RequireComponent(typeof(RoomGenerator), typeof(UnityEngine.AI.NavMeshSurface))]
public class LevelGenerator : MonoBehaviour {
    [SerializeField] private int LevelGridX      = 6;    // Arbitrary value, max 6x6 = 36 rooms?
    [SerializeField] private int LevelGridY      = 6;
    [SerializeField] private int roomMaxSize     = 20;   // Arbitrary value, max 20x20 room size?
    [SerializeField] private int roomSpawnOdds   = 3;
    [SerializeField] private int minRooms        = 3;
    [SerializeField] private int maxRooms        = 20;

    //3 x tiles = 6 units, each room need 1 tile of "exit space" to avoid paper thin walls, 
    // then the last tile in between is for the potential corridor
    const int UNITS_BETWEEN_ROOMS = 6;

    public string testSeed = "lmao";
    public string testResult;
    public string mapPreview;

    public RoomType generatorTestRoomType;
    System.Random random;

    UnityEngine.AI.NavMeshSurface navMesh;

    List<GeneratingRoom> generatingRooms = new List<GeneratingRoom>();

    // TODO: move to roomType
    // Squad
    [SerializeField]
    private GameObject squadPrefab;
    [SerializeField]
    private GameObject unitPrefab;
    [SerializeField]
    private MLAgents.Brain brain;
    [SerializeField]
    private Vector2Int minMaxAgents;
    [SerializeField]
    public GameObject playerReference;

    public int maxTries = 30;

    // ---- For generating 'overview' of where each room is supposed to be placed ---- //
    public int[,] GenerateLevelCoordinates( System.Random random )
    {
        int roomsGenerated = 0;
        int[,] tempCoordinates = new int[LevelGridX, LevelGridY];
        while (roomsGenerated < minRooms) {
            roomsGenerated = 0;
            for (int y = 0; y < LevelGridY; y++)
            {
                for (int x = 0; x < LevelGridX; x++)
                {
                    if (roomsGenerated >= maxRooms)
                    {
                        return tempCoordinates;
                    }
                    int spawnRoom = random.Next() % roomSpawnOdds;
                    if (spawnRoom == 0)
                    {
                        tempCoordinates[x, y] = 0; // Room
                        roomsGenerated++;
                    }
                    else
                    {
                        tempCoordinates[x, y] = 1; // No Room
                    }
                }
                testResult += "\r\n";
            }
        }
        return tempCoordinates;
    }

    // ---- For generating each "tile" in World Space, not only where each room is placed ---- //
    public int[,] GenerateTileCoordinates( System.Random rand )
    {
        int[,] tempCoordinates = new int[roomMaxSize * LevelGridX, roomMaxSize * LevelGridY];
        for (int y = 0; y < LevelGridY; y++)
        {
            for (int x = 0; x < LevelGridX; x++)
            {
                int roomRandom = random.Next() % 3;
                if (roomRandom == 0)
                {
                    for (int i = 0; i < roomMaxSize; i++)
                    {
                        for (int j = 0; j < roomMaxSize; j++)
                        {
                            tempCoordinates[(x * roomMaxSize) + i, (y * roomMaxSize) + j] = 0; // Room
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < roomMaxSize; i++)
                    {
                        for (int j = 0; j < roomMaxSize; j++)
                        {
                            tempCoordinates[(x * roomMaxSize) + j, (y * roomMaxSize) + i] = 1; // No Room
                        }
                    }
                }
            }
        }
        return tempCoordinates;
    }

    public void TestTileCoordinates( System.Random rand )
    {
        int[,] testCoordinates = new int[roomMaxSize * LevelGridX, roomMaxSize * LevelGridY];
        testCoordinates = GenerateTileCoordinates(rand);


        for (int y = 0; y < roomMaxSize * LevelGridY; y++)
        {
            for (int x = 0; x < roomMaxSize * LevelGridX; x++)
            {
                mapPreview += testCoordinates[x, y].ToString() + " ";
            }
            mapPreview += "\r\n";
        }
        //System.IO.File.WriteAllText(@"C:\Users\nikol\Documents\Skole\GameProgramming\TileMap.txt", mapPreview);
    }

    public void GenerateRoomsInWordspace( System.Random random, RoomType roomType, int[,] roomCoordinates )
    {
        for (int y = 0; y < LevelGridY; y++)
        {
            for (int x = 0; x < LevelGridX; x++)
            {
                if (roomCoordinates[x, y] == 0)
                {
                    //Vector2Int spawnPos = new Vector2Int((x * roomMaxSize)*2, (y * roomMaxSize)*2);
                    generatingRooms.Add(GetComponent<RoomGenerator>().GenerateRoom(random, roomType, Vector2Int.zero));
                }
            }
        }
        bool[] intersects = new bool[generatingRooms.Count]; //NOTE, this may be possible to optimize by multithreading
        for (int i = 1; i < generatingRooms.Count; i++)
        {
            int tries = 0; bool placed=false;
            for (/*tries = 0*/; tries < maxTries && !placed; tries++)
            {
                //Debug.Log($"room {i}, try #{tries + 1}");
                bool availableSpace = true;
                for (int j = 0; j < i && availableSpace; j++)
                {
                    if (generatingRooms[i].Bounds.Intersects(generatingRooms[j].Bounds, UNITS_BETWEEN_ROOMS))
                    {
                        availableSpace = false;

                        Vector2Int difference = Vector2Int.RoundToInt(generatingRooms[i].Bounds.center - generatingRooms[j].Bounds.center);
                        Vector2 direction = new Vector2(difference.x, difference.y).normalized;

                        float f = random.NextFloat() * 2 * Mathf.PI;
                        Vector2 randomDirection = new Vector2(Mathf.Cos(f), Mathf.Sin(f));
                        Vector2 biasedRandomDirection = randomDirection * Mathf.Sign(Vector2.Dot(randomDirection, direction));

                        //Apply offset for next test
                        Vector2Int offsetVector = generatingRooms[i].SnapToEdgeVector(generatingRooms[j], biasedRandomDirection, UNITS_BETWEEN_ROOMS);
                        //if (offsetVector.x % 2 != 0)
                            //Debug.LogAssertion($"bad offset {offsetVector} on room {i} with params: {generatingRooms[i].Bounds} \nsnapped to {j} with params {generatingRooms[j].Bounds}");
                        generatingRooms[i].Move(offsetVector);
                    }
                }
                if (availableSpace)
                    placed = true; //break by telling the room placer it's found a place, while also allowing to detect unplacable rooms
            }
            //Debug.Log($"New bounds set - pos: {generatingRooms[i].Bounds.position}, center: {generatingRooms[i].Bounds.center}, min: {generatingRooms[i].Bounds.min}");
            if (!placed)
            {
                GeneratingRoom unplacableRoom = generatingRooms[i];
                generatingRooms.Remove(unplacableRoom);
                unplacableRoom.Destroy();
            }
        }
        ConnectAll();
        foreach (var room in generatingRooms)
            room.Finalize();

        navMesh.BuildNavMesh();

        GameManager manager = FindObjectOfType<GameManager>();
        foreach (var room in generatingRooms)
        {
            SpawnGenerator.SpawnItemsInRoom(random, room.roomType, room.tileSet, room.floorTiles, room.wallTiles, room.Position + room.MaterializedPositionOffset);
            if(random.NextFloat() > 0)
            {
                SquadGenerator.CreateNewSquad(squadPrefab, unitPrefab, room.floorTiles, minMaxAgents, random, brain,
                                room.Root.GetComponent<Room>(), room.Position + room.MaterializedPositionOffset, playerReference);
            }
            if (manager != null && !manager.playerPositionSet)
            {
                manager.SetPlayerPosition(room.floorTiles, room.Position + room.MaterializedPositionOffset);
            }
        }
    }


    private void Start()
    {
        navMesh = GetComponent<UnityEngine.AI.NavMeshSurface>();
        generatingRooms.Clear();
        random = new Random(testSeed.GetHashCode());
        //int[,] roomPositions = GenerateLevelCoordinates(random);
        //GenerateRoomsInWordspace(random, generatorTestRoomType, roomPositions);
        // Commented out some code for now to make it work with the GenerateWithSeed call below from GameManager

        playerReference = GameObject.Find("Player");

        //UnityEngine.AI.NavMesh.AddNavMeshData(navMesh.navMeshData);
    }

    public void GenerateWithSeed(int seed, LevelType levelType)
    {
        random = new Random(seed);
        int[,] roomPositions = GenerateLevelCoordinates(random);
        GenerateRoomsInWordspace(random, levelType.levelInfo.roomTypes[0], roomPositions);
        UnityEngine.AI.NavMesh.AddNavMeshData(navMesh.navMeshData);
    }

    TreeNode<GeneratingRoom> mst;

    private void ConnectAll()
    {
        mst = new KruskalMinimumSpanningTree(generatingRooms).minimumSpanningTree;

        HashSet<Vector2Int> allCorridorTiles = new HashSet<Vector2Int>();
        HashSet<Vector2Int> allRoomTiles = new HashSet<Vector2Int>();

        foreach(GeneratingRoom room in generatingRooms)
            foreach (var tile in room.roomLayout.tilePositions)
                allRoomTiles.Add(room.FinalizedSpace(tile));

        int corridorCount = mst.CountTotalTreeNodes() - 1;
        HashSet<Vector2Int>[] corridorSets = new HashSet<Vector2Int>[corridorCount];
        GameObject[] corridors = new GameObject[corridorCount];
        GeneratingRoom[] corridorAssociatedRooms = new GeneratingRoom[corridorCount];
        GeneratingRoom[] corridorAssociatedNeighbours = new GeneratingRoom[corridorCount];
        int corridorIndex = 0;
        //not so crazy lambda anymore, but it's doing a lot!!!
        mst.Traverse(n => {
            for (int i = 0; i < n.ChildCount; i++, corridorIndex++)
            {
                var neighbour = n.GetChild(i).Value;


                corridorSets[corridorIndex] = ConnectRooms(n.Value, neighbour); //get the wanted path for corridors in the form of an hashset of the tiles on the way over.
                corridorSets[corridorIndex].ExceptWith(allCorridorTiles); //if some other corridor already uses tiles in this, then by all means, let the other corridor keep said tiles!
                allCorridorTiles.UnionWith(corridorSets[corridorIndex]); //register these tiles up in "all tiles" so other tiles also need to consider this corridors tiles in this ^

                corridorAssociatedRooms[corridorIndex] = n.Value;
                corridorAssociatedNeighbours[corridorIndex] = neighbour;

                //Filter loop, ensure corridors entering rooms are sivilized and use doors
                foreach (var t in corridorSets[corridorIndex]) 
                {
                    if (allRoomTiles.Contains(t))
                    {
                        foreach(GeneratingRoom room in generatingRooms)
                        {
                            Vector2Int unitSpaceTile = room.UnitSpace(t);
                            if (room.eastWalls.Contains(unitSpaceTile))
                            {
                                for (int tileIndex = 0; tileIndex < room.roomLayout.tilePositions.Length; tileIndex++)
                                {
                                    if(room.roomLayout.tilePositions[tileIndex] == unitSpaceTile)
                                    {
                                        Vector3 pos = room.roomLayout.tiles[tileIndex].transform.position;
#if UNITY_EDITOR
                                        DestroyImmediate(
#else
                                        Destroy(
#endif
                                            room.roomLayout.tiles[tileIndex]
                                        );
                                        room.roomLayout.tiles[tileIndex] = Instantiate(room.tileSet.Doors_East.GetRandom(random));
                                        room.roomLayout.tiles[tileIndex].transform.position = pos;
                                        break;
                                    }
                                }
                                break;
                            } else if (room.westWalls.Contains(unitSpaceTile))
                            {
                                for (int tileIndex = 0; tileIndex < room.roomLayout.tilePositions.Length; tileIndex++)
                                {
                                    if (room.roomLayout.tilePositions[tileIndex] == unitSpaceTile)
                                    {
                                        Vector3 pos = room.roomLayout.tiles[tileIndex].transform.position;
#if UNITY_EDITOR
                                        DestroyImmediate(
#else
                                        Destroy(
#endif
                                            room.roomLayout.tiles[tileIndex]
                                        );
                                        room.roomLayout.tiles[tileIndex] = Instantiate(room.tileSet.Doors_West.GetRandom(random));
                                        room.roomLayout.tiles[tileIndex].transform.position = pos;
                                        break;
                                    }
                                }
                                break;
                            } else if (room.northWalls.Contains(unitSpaceTile))
                            {
                                for (int tileIndex = 0; tileIndex < room.roomLayout.tilePositions.Length; tileIndex++)
                                {
                                    if (room.roomLayout.tilePositions[tileIndex] == unitSpaceTile)
                                    {
                                        Vector3 pos = room.roomLayout.tiles[tileIndex].transform.position;
#if UNITY_EDITOR
                                        DestroyImmediate(
#else
                                        Destroy(
#endif
                                            room.roomLayout.tiles[tileIndex]
                                        );
                                        room.roomLayout.tiles[tileIndex] = Instantiate(room.tileSet.Doors_North.GetRandom(random));
                                        room.roomLayout.tiles[tileIndex].transform.position = pos;
                                        break;
                                    }
                                }
                                break;
                            } else if (room.southWalls.Contains(unitSpaceTile))
                            {
                                for (int tileIndex = 0; tileIndex < room.roomLayout.tilePositions.Length; tileIndex++)
                                {
                                    if (room.roomLayout.tilePositions[tileIndex] == unitSpaceTile)
                                    {
                                        Vector3 pos = room.roomLayout.tiles[tileIndex].transform.position;
#if UNITY_EDITOR
                                        DestroyImmediate(
#else
                                        Destroy(
#endif
                                            room.roomLayout.tiles[tileIndex]
                                        );
                                        room.roomLayout.tiles[tileIndex] = Instantiate(room.tileSet.Doors_South.GetRandom(random));
                                        room.roomLayout.tiles[tileIndex].transform.position = pos;
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
                corridorSets[corridorIndex].ExceptWith(allRoomTiles);
                corridors[corridorIndex] = new GameObject($"corridor{corridorIndex}_{n.Value.Root.name}_{neighbour.Root.name}");
                //TODO combine the tiles under n.Value
            }
        });

        //actually assign the corridor tiles
        for (corridorIndex = 0; corridorIndex < corridorCount; corridorIndex++)
        {
            GeneratingRoom roomA = corridorAssociatedRooms[corridorIndex], roomB = corridorAssociatedNeighbours[corridorIndex];
            RoomType.RoomSet tileSet = random.NextBool() ? roomA.tileSet : roomB.tileSet;
            GameObject[] corridorTiles = new GameObject[corridorSets[corridorIndex].Count];
            int corridorTile = 0;
            foreach (var t in corridorSets[corridorIndex])
            {
                TileType tileTypeMask = TileType.FLOOR;

                if (!allCorridorTiles.Contains(t + Vector2Int.right * 2))
                    tileTypeMask |= TileType.WALL_EAST; //TODO check for adjacent room tile
                if (!allCorridorTiles.Contains(t + Vector2Int.up * 2))
                    tileTypeMask |= TileType.WALL_NORTH;
                if (!allCorridorTiles.Contains(t + Vector2Int.left * 2))
                    tileTypeMask |= TileType.WALL_WEST;
                if (!allCorridorTiles.Contains(t + Vector2Int.down * 2))
                    tileTypeMask |= TileType.WALL_SOUTH;
                
                GameObject targetTile;
                switch (tileTypeMask)
                {
                    case TileType.FLOOR:
                    targetTile = tileSet.Floors.GetRandom(random);
                    break;
                    case TileType.WALL_SOUTH:
                    targetTile = tileSet.Walls_South.GetRandom(random);
                    break;
                    case TileType.WALL_WEST:
                    targetTile = tileSet.Walls_West.GetRandom(random);
                    break;
                    case TileType.CORNER_SOUTHWEST:
                    targetTile = tileSet.Corners_SouthWest.GetRandom(random);
                    break;
                    case TileType.WALL_NORTH:
                    targetTile = tileSet.Walls_North.GetRandom(random);
                    break;
                    case TileType.CORRIDOR_EASTWEST:
                    targetTile = tileSet.Corridors_EastWest.GetRandom(random);
                    break;
                    case TileType.CORNER_NORTHWEST:
                    targetTile = tileSet.Corners_NorthWest.GetRandom(random);
                    break;
                    case TileType.WALL_EAST:
                    targetTile = tileSet.Walls_East.GetRandom(random);
                    break;
                    case TileType.CORNER_SOUTHEAST:
                    targetTile = tileSet.Corners_SouthEast.GetRandom(random);
                    break;
                    case TileType.CORRIDOR_NORTHSOUTH:
                    targetTile = tileSet.Corridors_NorthSouth.GetRandom(random);
                    break;
                    case TileType.CORNER_NORTHEAST:
                    targetTile = tileSet.Corners_NorthEast.GetRandom(random);
                    break;
                    default:
                    Debug.Log($"Tile Type was: {tileTypeMask}");
                    //TODO resolve this, either by making the tile a valid tile: corridor end caps
                    //Debug.LogWarning($"Tile: {t} unable to assign tile type, UNUSED combination: {tileTypeMask}, Added as cornerTile to make sure the bounds are set correctly");
                    //query the rooms of
                    Debug.DrawLine(new Vector3(t.x, 0, t.y), new Vector3(t.x, 20, t.y), Color.black, 20);
                    Debug.DrawLine(new Vector3(t.x, 0, t.y), new Vector3(t.x, 20, t.y), Color.black, 20);
                    if (roomA.allTiles.Contains(roomA.UnitSpace(t) + Vector2Int.right) || roomA.allTiles.Contains(roomA.UnitSpace(t) + Vector2Int.left))
                        targetTile = tileSet.Corridors_EastWest.GetRandom(random);
                    else if (roomA.allTiles.Contains(roomA.UnitSpace(t) + Vector2Int.up) || roomA.allTiles.Contains(roomA.UnitSpace(t) + Vector2Int.down))
                        targetTile = tileSet.Corridors_NorthSouth.GetRandom(random);
                    else if (roomB.allTiles.Contains(roomB.UnitSpace(t) + Vector2Int.right) || roomB.allTiles.Contains(roomB.UnitSpace(t) + Vector2Int.left))
                        targetTile = tileSet.Corridors_EastWest.GetRandom(random);
                    else if (roomB.allTiles.Contains(roomB.UnitSpace(t) + Vector2Int.up) || roomB.allTiles.Contains(roomB.UnitSpace(t) + Vector2Int.down))
                        targetTile = tileSet.Corridors_NorthSouth.GetRandom(random);
                    else
                        targetTile = tileSet.Floors.GetRandom(random);
                    break;
                }
                corridorTiles[corridorTile++] = Instantiate(targetTile, new Vector3(t.x, 0,t.y), Quaternion.identity, corridors[corridorIndex].transform);
            }
            CombineCorridorTiles(corridorTiles, corridors[corridorIndex]);
        }
    }

    private void CombineCorridorTiles(GameObject[] tiles, GameObject root) //TODO Generalize for all uses
    {
        MeshFilter[] filters = tiles.Select(t => t.GetComponentInChildren<MeshFilter>()).ToArray();
        MeshRenderer[] renderers = tiles.Select(t => t.GetComponentInChildren<MeshRenderer>()).ToArray();

        HashSet<Material> materials = new HashSet<Material>();
        foreach (MeshRenderer r in renderers)
        {
            foreach (Material m in r.sharedMaterials)
            {
                if (!materials.Contains(m))
                    materials.Add(m);
            }
        }
        List<Mesh> submeshes = new List<Mesh>(materials.Count);
        foreach (Material m in materials)
        {
            List<CombineInstance> combiner = new List<CombineInstance>(tiles.Length);
            foreach (MeshFilter f in filters)
            {
                Renderer r = f.GetComponent<MeshRenderer>();
                Debug.Assert(r != null, $"Filter:{f} has no renderer, HOW COULD THIS HAPPEN TO ME?");
                Material[] meshMaterials = r.sharedMaterials;
                for (int matIndex = 0; matIndex < meshMaterials.Length; matIndex++)
                {
                    if (meshMaterials[matIndex] != m)
                        continue;
                    combiner.Add(new CombineInstance()
                    {
                        mesh = f.sharedMesh,
                        subMeshIndex = matIndex,
                        transform = f.transform.localToWorldMatrix
                    });
                }
            }
            Mesh mesh = new Mesh();
            mesh.CombineMeshes(combiner.ToArray(), true);
            submeshes.Add(mesh);
        }
        foreach (GameObject go in tiles)
        {
#if UNITY_EDITOR
            UnityEngine.Object.DestroyImmediate(go);
#else
            Destroy(go);
#endif
        }
        CombineInstance[] finalCombiner = submeshes.Select(
            m => new CombineInstance() { mesh = m, subMeshIndex = 0, transform = Matrix4x4.identity }
        ).ToArray();
        Mesh finalMesh = new Mesh();
        finalMesh.CombineMeshes(finalCombiner, false);

        root.AddComponent<MeshFilter>().sharedMesh = finalMesh;
        root.AddComponent<MeshCollider>().sharedMesh = finalMesh;
        var renderer = root.AddComponent<MeshRenderer>();
        renderer.sharedMaterials = materials.ToArray();
        renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.TwoSided;
    }





    class TracebackNode
    {
        public Vector2Int tile;
        public TracebackNode start;
        public Vector2Int direction;
        public override int GetHashCode() => tile.GetHashCode();
    };
    struct InternalSearchNode : IComparable<InternalSearchNode>
    {
        public Vector2Int tile;
        public Vector2Int nextStep;
        public int weight; //the set is sorted after least weight, i.e. the closest to end

        public int CompareTo(InternalSearchNode other)
        {
            int v = weight.CompareTo(other.weight);
            return (v != 0) ? v : GetHashCode().CompareTo(other.GetHashCode());
        }

        public override int GetHashCode() => tile.GetHashCode();
        
    };
    private HashSet<Vector2Int> ConnectRooms(GeneratingRoom room1, GeneratingRoom room2)
    {
        HashSet<Vector2Int> corridorTiles = new HashSet<Vector2Int>();
        SortedSet<InternalSearchNode> openSet = new SortedSet<InternalSearchNode>();
        HashSet<Vector2Int> closedSet = new HashSet<Vector2Int>();

        Vector2 relative = room2.Position - room1.Position;
        int dir = 0;
        //TODO improve
        if (Mathf.Abs(relative.x) > Mathf.Abs(relative.y))
            dir = (Mathf.Sign(relative.x) > 0) ? 1 : 3;
        else
            dir = (Mathf.Sign(relative.y) > 0) ? 0 : 2;


        //TODO handle holes in the room (we can probably discard them from the wall-sets)

        #region GetRoomExits
        Vector2Int start = Vector2Int.zero, end = Vector2Int.zero;
        Vector2Int startDirection = Vector2Int.zero, endDirection = Vector2Int.zero;
        HashSet<Vector2Int> wallToPlaceDoorOn = new HashSet<Vector2Int>();
        switch (dir)
        {
            case 0:
                {
                    startDirection = Vector2Int.up;
                    wallToPlaceDoorOn = new HashSet<Vector2Int>(room1.northWalls);
                    do //TODO change to while loop risk of no item in collection
                    {
                        start = wallToPlaceDoorOn.GetRandom(random);
                        wallToPlaceDoorOn.Remove(start);
                    }
                    while (
                            (
                                room1.allTiles.Contains(start + startDirection) ||
                                room1.allTiles.Contains(start + startDirection + new Vector2Int(-startDirection.y, startDirection.x)) ||
                                room1.allTiles.Contains(start + startDirection + new Vector2Int(startDirection.y, -startDirection.x)) ||
                                room1.allTiles.Contains(start + startDirection * 2) ||
                                room1.allTiles.Contains(start + startDirection * 2 + new Vector2Int(-startDirection.y, startDirection.x)) ||
                                room1.allTiles.Contains(start + startDirection * 2 + new Vector2Int(startDirection.y, -startDirection.x)) ||
                                room1.allTiles.Contains(start + startDirection * 3)
                            ) && wallToPlaceDoorOn.Count > 0 //TODO what about cases where none of the tiles in wallToPlaceDoorOn are usable?
                    );

                    endDirection = Vector2Int.down;
                    wallToPlaceDoorOn = new HashSet<Vector2Int>(room2.southWalls);
                    do
                    {
                        end = wallToPlaceDoorOn.GetRandom(random);
                        wallToPlaceDoorOn.Remove(end);
                    }
                    while (
                            (
                                room2.allTiles.Contains(end + endDirection) ||
                                room2.allTiles.Contains(end + endDirection + new Vector2Int(-endDirection.y, endDirection.x)) ||
                                room2.allTiles.Contains(end + endDirection + new Vector2Int(endDirection.y, -endDirection.x)) ||
                                room2.allTiles.Contains(end + endDirection * 2) ||
                                room2.allTiles.Contains(end + endDirection * 2 + new Vector2Int(-endDirection.y, endDirection.x)) ||
                                room2.allTiles.Contains(end + endDirection * 2 + new Vector2Int(endDirection.y, -endDirection.x)) ||
                                room1.allTiles.Contains(end + endDirection * 3)
                            ) && wallToPlaceDoorOn.Count > 0
                    );
                    break;
                }
            case 1:
                {
                    startDirection = Vector2Int.right;
                    wallToPlaceDoorOn = new HashSet<Vector2Int>(room1.eastWalls);
                    do
                    {
                        start = wallToPlaceDoorOn.GetRandom(random); //TODO make sure to pick another wall when the set is empty
                        wallToPlaceDoorOn.Remove(start);
                    }
                    while (
                            (
                                room1.allTiles.Contains(start + startDirection) ||
                                room1.allTiles.Contains(start + startDirection + new Vector2Int(-startDirection.y, startDirection.x)) ||
                                room1.allTiles.Contains(start + startDirection + new Vector2Int(startDirection.y, -startDirection.x)) ||
                                room1.allTiles.Contains(start + startDirection * 2) ||
                                room1.allTiles.Contains(start + startDirection * 2 + new Vector2Int(-startDirection.y, startDirection.x)) ||
                                room1.allTiles.Contains(start + startDirection * 2 + new Vector2Int(startDirection.y, -startDirection.x)) || 
                                room1.allTiles.Contains(start + startDirection * 3)
                            ) && wallToPlaceDoorOn.Count > 0
                    );

                    endDirection = Vector2Int.left;
                    wallToPlaceDoorOn = new HashSet<Vector2Int>(room2.westWalls);
                    do
                    {
                        end = wallToPlaceDoorOn.GetRandom(random);
                        wallToPlaceDoorOn.Remove(end);
                    }
                    while (
                            (
                                room2.allTiles.Contains(end + endDirection) ||
                                room2.allTiles.Contains(end + endDirection + new Vector2Int(-endDirection.y, endDirection.x)) ||
                                room2.allTiles.Contains(end + endDirection + new Vector2Int(endDirection.y, -endDirection.x)) ||
                                room2.allTiles.Contains(end + endDirection * 2) ||
                                room2.allTiles.Contains(end + endDirection * 2 + new Vector2Int(-endDirection.y, endDirection.x)) ||
                                room2.allTiles.Contains(end + endDirection * 2 + new Vector2Int(endDirection.y, -endDirection.x)) ||
                                room1.allTiles.Contains(end + endDirection * 3)
                            ) && wallToPlaceDoorOn.Count > 0
                    );
                    break;
                }
            case 2:
                {
                    startDirection = Vector2Int.down;
                    wallToPlaceDoorOn = new HashSet<Vector2Int>(room1.southWalls);
                    do
                    {
                        start = wallToPlaceDoorOn.GetRandom(random);
                        wallToPlaceDoorOn.Remove(start);
                    }
                    while (
                            (
                                room1.allTiles.Contains(start + startDirection) ||
                                room1.allTiles.Contains(start + startDirection + new Vector2Int(-startDirection.y, startDirection.x)) ||
                                room1.allTiles.Contains(start + startDirection + new Vector2Int(startDirection.y, -startDirection.x)) ||
                                room1.allTiles.Contains(start + startDirection * 2) ||
                                room1.allTiles.Contains(start + startDirection * 2 + new Vector2Int(-startDirection.y, startDirection.x)) ||
                                room1.allTiles.Contains(start + startDirection * 2 + new Vector2Int(startDirection.y, -startDirection.x)) ||
                                room1.allTiles.Contains(start + startDirection * 3)
                            ) && wallToPlaceDoorOn.Count > 0
                    );

                    endDirection = Vector2Int.up;
                    wallToPlaceDoorOn = new HashSet<Vector2Int>(room2.northWalls);
                    do
                    {
                        end = wallToPlaceDoorOn.GetRandom(random);
                        wallToPlaceDoorOn.Remove(end);
                    }
                    while (
                            (
                                room2.allTiles.Contains(end + endDirection) ||
                                room2.allTiles.Contains(end + endDirection + new Vector2Int(-endDirection.y, endDirection.x)) ||
                                room2.allTiles.Contains(end + endDirection + new Vector2Int(endDirection.y, -endDirection.x)) ||
                                room2.allTiles.Contains(end + endDirection * 2) ||
                                room2.allTiles.Contains(end + endDirection * 2 + new Vector2Int(-endDirection.y, endDirection.x)) ||
                                room2.allTiles.Contains(end + endDirection * 2 + new Vector2Int(endDirection.y, -endDirection.x)) ||
                                room1.allTiles.Contains(end + endDirection * 3)
                            ) && wallToPlaceDoorOn.Count > 0
                    );
                    break;
                }
            case 3:
                {
                    startDirection = Vector2Int.left;
                    wallToPlaceDoorOn = new HashSet<Vector2Int>(room1.westWalls);
                    do
                    {
                        start = wallToPlaceDoorOn.GetRandom(random);
                        wallToPlaceDoorOn.Remove(start);
                    }
                    while (
                            (
                                room1.allTiles.Contains(start + startDirection) ||
                                room1.allTiles.Contains(start + startDirection + new Vector2Int(-startDirection.y, startDirection.x)) ||
                                room1.allTiles.Contains(start + startDirection + new Vector2Int(startDirection.y, -startDirection.x)) ||
                                room1.allTiles.Contains(start + startDirection * 2) ||
                                room1.allTiles.Contains(start + startDirection * 2 + new Vector2Int(-startDirection.y, startDirection.x)) ||
                                room1.allTiles.Contains(start + startDirection * 2 + new Vector2Int(startDirection.y, -startDirection.x)) ||
                                room1.allTiles.Contains(start + startDirection * 3)
                            ) && wallToPlaceDoorOn.Count > 0
                    );

                    endDirection = Vector2Int.right;
                    wallToPlaceDoorOn = new HashSet<Vector2Int>(room2.eastWalls);
                    do
                    {
                        end = wallToPlaceDoorOn.GetRandom(random);
                        wallToPlaceDoorOn.Remove(end);
                    }
                    while (
                            (
                                room2.allTiles.Contains(end + endDirection) ||
                                room2.allTiles.Contains(end + endDirection + new Vector2Int(-endDirection.y, endDirection.x)) ||
                                room2.allTiles.Contains(end + endDirection + new Vector2Int(endDirection.y, -endDirection.x)) ||
                                room2.allTiles.Contains(end + endDirection * 2) ||
                                room2.allTiles.Contains(end + endDirection * 2 + new Vector2Int(-endDirection.y, endDirection.x)) ||
                                room2.allTiles.Contains(end + endDirection * 2 + new Vector2Int(endDirection.y, -endDirection.x)) ||
                                room1.allTiles.Contains(end + endDirection * 3)
                            ) && wallToPlaceDoorOn.Count > 0
                    );
                    break;
                }
        }

        Debug.DrawLine(room1.Finalized3DSpace(start), room1.Finalized3DSpace(start) + Vector3.up * 3, Color.magenta, 40);
        Debug.DrawLine(room2.Finalized3DSpace(end), room2.Finalized3DSpace(end) + Vector3.up * 3, Color.black, 40);

        #endregion
        Vector2Int externalStart, externalEnd;
        corridorTiles = InternalCorridorSearch(room1, start, startDirection, openSet, closedSet, out externalStart); //start room
        corridorTiles.UnionWith(InternalCorridorSearch(room2, end, endDirection, openSet, closedSet, out externalEnd));//connected room, i.e. neighbour


        corridorTiles.UnionWith(ExternalCorridorSearch(externalStart, externalEnd));

        Debug.DrawLine(
                            new Vector3(externalStart.x, 5, externalStart.y),
                            new Vector3(externalEnd.x, 5, externalEnd.y),
                            Color.cyan,
                            20
                        );
        return corridorTiles;
    }


    private HashSet<Vector2Int> InternalCorridorSearch(GeneratingRoom room, Vector2Int unitSpaceStart, Vector2Int direction, SortedSet<InternalSearchNode> open, HashSet<Vector2Int> closed, out Vector2Int exitTile)
    {
        HashSet<Vector2Int> internalTiles = new HashSet<Vector2Int>();
        open.Clear();
        closed.Clear();

        RectInt bounds = new RectInt(Vector2Int.zero, room.Bounds.size);
        Vector2Int internalStart = room.MaterializedSpace(unitSpaceStart);
        Vector2Int internalEnd = Vector2Int.zero;
        if (Mathf.Abs(direction.x) > 0) //optimize -> vertical/horizontal
            internalEnd = new Vector2Int((direction.x > 0) ? bounds.size.x + 4 : -4, internalStart.y);
        else
            internalEnd = new Vector2Int(internalStart.x, (direction.y > 0) ? bounds.size.y + 4 : -4);

        exitTile = room.Position + internalStart + direction * 4;

        if (bounds.ContainsInclusive(internalStart + direction * 2))
        {
            TracebackNode start = new TracebackNode() { tile = internalStart + direction * 2, start = null, direction = direction * -2 };
            open.Add(new InternalSearchNode() { tile = start.tile, nextStep = direction * 2, weight = 0 });
            for (int k = 0; open.Count > 0 && k < 5; k++)
            {
                var current = open.Min;
                open.Remove(current);
                closed.Add(current.tile);
                
                int steps = 1; //step counter, resets upon direction change
                //current position
                Debug.DrawLine(
                    new Vector3(room.Position.x + current.tile.x, 0, room.Position.y + current.tile.y),
                    new Vector3(room.Position.x + current.tile.x, 10, room.Position.y + current.tile.y),
                        Color.white,
                        10
                );
                ////backtrack
                //Debug.DrawLine(
                //    new Vector3(room.Position.x + current.x, 4, room.Position.y + current.tile.y),
                //    new Vector3(room.Position.x + current.x - nextStep.x * current.stepsBeforeTurn, 4, room.Position.y + current.tile.y - current.nextStep.y * current.stepsBeforeTurn),
                //    Color.blue,
                //    25
                //);

                Vector2Int next = current.tile + current.nextStep;
                while (//jumping forward to last valid tile
                   !closed.Contains(next) && bounds.ContainsInclusive(next)
                )
                {
                    Vector2Int
                        leftOfNext      = new Vector2Int(next.x - current.nextStep.y, next.y + current.nextStep.x),
                        rightOfNext     = new Vector2Int(next.x + current.nextStep.y, next.y - current.nextStep.x),
                        forwardOfNext   = next + current.nextStep;

                    if( // bad corridor position -> Stop jumping in that direction
                        room.allTiles.Contains(new Vector2Int((forwardOfNext.x - room.MaterializedPositionOffset.x) / 2, (forwardOfNext.y - room.MaterializedPositionOffset.y) / 2)) ||
                        room.allTiles.Contains(new Vector2Int((rightOfNext.x - room.MaterializedPositionOffset.x) / 2, (rightOfNext.y - room.MaterializedPositionOffset.y) / 2)) ||
                        room.allTiles.Contains(new Vector2Int((leftOfNext.x - room.MaterializedPositionOffset.x) / 2, (leftOfNext.y - room.MaterializedPositionOffset.y) / 2))
                      ) { next -= current.nextStep; break; }

                    if (!closed.Contains(leftOfNext))
                    { 
                        Vector2Int diff = leftOfNext - internalEnd;
                        open.Add( new InternalSearchNode() {
                            tile = leftOfNext, weight = (Mathf.Abs(diff.x) + Mathf.Abs(diff.y)),
                            nextStep = new Vector2Int(-current.nextStep.y, current.nextStep.x)
                        });
                        Debug.DrawLine(
                            new Vector3(room.Position.x + next.x, 4.1f, room.Position.y + next.y),
                            new Vector3(room.Position.x + leftOfNext.x, 4.1f, room.Position.y + leftOfNext.y),
                            Color.cyan,
                            15
                        );
                    }
                    if (!closed.Contains(rightOfNext))
                    {
                        Vector2Int diff = rightOfNext - internalEnd;
                        open.Add(new InternalSearchNode() {
                            tile = rightOfNext, weight =(Mathf.Abs(diff.x) + Mathf.Abs(diff.y)),
                            nextStep = new Vector2Int(current.nextStep.y, -current.nextStep.x)
                        });
                        Debug.DrawLine(
                            new Vector3(room.Position.x + next.x, 4.1f, room.Position.y + next.y),
                            new Vector3(room.Position.x + rightOfNext.x, 4.1f, room.Position.y + rightOfNext.y),
                            Color.magenta,
                            15
                        );
                    }
                    steps++;
                    next += current.nextStep;
                }
                start = new TracebackNode() { tile = next, start = start, direction = current.nextStep * -1 }; // move start position of jump

                //STOP, COLLISION!
                Debug.DrawLine(
                        new Vector3(room.Position.x + next.x, 0,  room.Position.y + next.y),
                        new Vector3(room.Position.x + next.x, 10, room.Position.y + next.y),
                            Color.red,
                            10
                    );
                Debug.DrawLine(
                       new Vector3(room.Position.x + next.x - current.nextStep.y, 6, room.Position.y + next.y + current.nextStep.x),
                       new Vector3(room.Position.x + next.x + current.nextStep.y, 6, room.Position.y + next.y - current.nextStep.x),
                           Color.red,
                           10
                   );


                //from stop position back the distance traveled
                Debug.DrawLine(
                        new Vector3(room.Position.x + next.x - current.nextStep.x, 6, room.Position.y + next.y - current.nextStep.y),
                        new Vector3(room.Position.x + next.x - current.nextStep.x * steps, 6, room.Position.y + next.y - current.nextStep.y * steps),
                            Color.black,
                            10
                    );

                if (!room.Bounds.ContainsInclusive(room.Position + next)) //if the search leaves the bounds, use the tile where the path left bounds, optimally this is equal to internalEnd
                {
                    Debug.DrawLine(
                        new Vector3(room.Position.x + next.x, 0, room.Position.y + next.y),
                        new Vector3(room.Position.x + next.x, 10, room.Position.y + next.y),
                            Color.green,
                            10
                    );
                    internalEnd = next;
                    exitTile = room.Position + next + current.nextStep; //TODO
                    TracebackNode tracebackPointer = start;
                    for ( ; tracebackPointer.start != null; tracebackPointer = tracebackPointer.start)
                    {
                        for(Vector2Int tile = tracebackPointer.tile; tile != tracebackPointer.start.tile; tile += tracebackPointer.direction)
                        {
                            internalTiles.Add(room.Position + tile);
                        }
                            Debug.DrawLine(
                                new Vector3(room.Position.x + tracebackPointer.tile.x, 6, room.Position.y + tracebackPointer.tile.y),
                                new Vector3(room.Position.x + tracebackPointer.start.tile.x, 6, room.Position.y + tracebackPointer.start.tile.y),
                                Color.grey,
                                20
                            );
                    }
                    break;
                }
            }
        }
        internalTiles.Add(room.Position + internalStart + direction * 2);

        for (int i = 0; i < room.roomLayout.tilePositions.Length; i++)
        {
            if(room.roomLayout.tilePositions[i] == unitSpaceStart)
            {
                Vector3 pos = room.roomLayout.tiles[i].transform.position;
#if UNITY_EDITOR
                DestroyImmediate(
#else
                Destroy(
#endif
                    room.roomLayout.tiles[i]
                );

                if (Mathf.Abs(direction.x) > 0)
                {
                    if (Mathf.Sign(direction.x) > 0)
                        room.roomLayout.tiles[i] = Instantiate(room.tileSet.Doors_East.GetRandom(random));
                    else
                        room.roomLayout.tiles[i] = Instantiate(room.tileSet.Doors_West.GetRandom(random));
                }
                else
                {
                    if (Mathf.Sign(direction.y) > 0)
                        room.roomLayout.tiles[i] = Instantiate(room.tileSet.Doors_North.GetRandom(random));
                    else
                        room.roomLayout.tiles[i] = Instantiate(room.tileSet.Doors_South.GetRandom(random));
                }
                room.roomLayout.tiles[i].transform.position = pos;
                break;
            }
        }

        return internalTiles;
    }

    //private delegate bool LineIntersector(Vector2Int, Vector2Int, Vector2Int, Vector2Int, ref Vector2Int)

    private HashSet<Vector2Int> ExternalCorridorSearch(Vector2Int start, Vector2Int end)
    {
        HashSet<Vector2Int> externalTiles = new HashSet<Vector2Int>();
        
        Vector2Int current = start;
        Vector2 relation = end - current;
        Vector2Int next = Vector2Int.zero;
        Vector2Int dir = Vector2Int.zero;        
        
        //TODO use some pathfinding to avoid going through rooms

        bool verticalFirst = random.NextBool();

        next = verticalFirst ? new Vector2Int(current.x, (end.y / 2) - ((end.y / 2) % 2)) : new Vector2Int((end.x / 2) - ((end.x / 2) % 2), current.y);

        externalTiles.Add(current);
        
        next = verticalFirst ? new Vector2Int(end.x, current.y) : new Vector2Int(current.x, end.y);
        dir = next - current;
        dir.Clamp(new Vector2Int(-1, -1), Vector2Int.one);
        Debug.DrawLine(
                new Vector3(current.x, 0, current.y),
                new Vector3(current.x, 20, current.y),
                Color.yellow,
                20
            );
        while (current != next)//walk to wall
        {
            current += dir * 2;
            externalTiles.Add(current);
            Debug.DrawLine(
                new Vector3(current.x, 0, current.y),
                new Vector3(current.x, 20, current.y),
                Color.yellow,
                20
            );
        }
        dir = end - current;
        dir.Clamp(new Vector2Int(-1, -1), Vector2Int.one);
        while (current != end)//walk to end
        {
            current += dir*2;
            externalTiles.Add(current);
            Debug.DrawLine(
                new Vector3(current.x, 0, current.y),
                new Vector3(current.x, 20, current.y),
                Color.yellow,
                20
            ); 
        }
        return externalTiles;
    }


#if UNITY_EDITOR
    [UnityEditor.MenuItem("Tools/LevelGeneration/Try MockGenLevel %g")]
    public static void TryMockGenerate()
    {
        FindObjectOfType<LevelGenerator>()?.MockGenerate();
    }

    [UnityEditor.MenuItem("Tools/Test Maths %m")]
    public static void TestMaths()
    {
    }
#endif

    [ContextMenu("Mock Generate")]
    private void MockGenerate()
    {
        navMesh = GetComponent<UnityEngine.AI.NavMeshSurface>();
        generatingRooms.Clear();
        random = new Random(testSeed.GetHashCode());
        int[,] roomPositions = GenerateLevelCoordinates(random);
        GenerateRoomsInWordspace(random, generatorTestRoomType, roomPositions);
        //generatedRooms.Add(GetComponent<RoomGenerator>().GenerateRoomSpanningRects(random, generatorTestRoomType, Vector2Int.zero));
    }

#if UNITY_EDITOR
    void DrawRoomGizmos(GeneratingRoom room)
    {
        //Gizmos.color = new Color(0.5f, 1, 0, 0.5f);
        //Gizmos.DrawCube(
        //    new Vector3(room.Bounds.center.x, 1, room.Bounds.center.y),
        //    new Vector3(room.Bounds.size.x + 8, 2, room.Bounds.size.y + 8) //12 = 6*2 - i.e. offsets on both sides of the rect
        //);
        Gizmos.color = new Color(0.5f, 1, 0, 0.5f);
        foreach (var item in room.northWalls)
        {
            Gizmos.DrawCube(room.Root.position + new Vector3(item.x*2 + room.MaterializedPositionOffset.x, 0, item.y* 2 + room.MaterializedPositionOffset.y), Vector3.one);
        }
        Gizmos.color = new Color(0f, 1, 0.5f, 0.5f);
        foreach (var item in room.southWalls)
        {
            Gizmos.DrawCube(room.Root.position + new Vector3(item.x * 2 + room.MaterializedPositionOffset.x, 0, item.y * 2 + room.MaterializedPositionOffset.y), Vector3.one);
        }
        Gizmos.color = new Color(1, 0.5f, 0, 0.5f);
        foreach (var item in room.westWalls)
        {
            Gizmos.DrawCube(room.Root.position + new Vector3(item.x * 2 + room.MaterializedPositionOffset.x, 0, item.y * 2 + room.MaterializedPositionOffset.y), Vector3.one);
        }
        Gizmos.color = new Color(0.5f, 1, 0.5f, 0.5f);
        foreach (var item in room.eastWalls)
        {
            Gizmos.DrawCube(room.Root.position + new Vector3(item.x * 2 + room.MaterializedPositionOffset.x, 0, item.y * 2 + room.MaterializedPositionOffset.y), Vector3.one);
        }
        Gizmos.color = new Color(0, 0, 0, 1);
        foreach (var item in room.cornerTiles)
        {
            Gizmos.DrawCube(room.Root.position + new Vector3(item.x * 2 + room.MaterializedPositionOffset.x, 0, item.y * 2 + room.MaterializedPositionOffset.y), Vector3.one);
        }
        Gizmos.color = new Color(1, 0, 0, 0.8f);

        Gizmos.DrawCube(
            new Vector3(room.Bounds.center.x, 1, room.Bounds.center.y),
            new Vector3(room.Bounds.size.x, 2, room.Bounds.size.y)
        );

        Gizmos.DrawWireCube(
            new Vector3(room.Bounds.center.x, 1, room.Bounds.center.y),
            new Vector3(room.Bounds.size.x+6, 1, room.Bounds.size.y+6)
        );
        //Gizmos.color = new Color(0, 0.7f, 0.5f, 0.8f);
    }
    void DrawGizmosLineToChild(GeneratingRoom parent, int i, GeneratingRoom child, int l)
    {
        Vector2 o = parent.Bounds.center + (Vector2)(child.Position - parent.Position) * 0.5f;
        //UnityEditor.Handles.Label(new Vector3(o.x,0,o.y), $"from {i} to {l}");

        Gizmos.DrawLine(
            new Vector3(parent.Bounds.center.x, 0, parent.Bounds.center.y), 
            new Vector3(child.Bounds.center.x, 0, child.Bounds.center.y)
        );
    }
    
    public void OnDrawGizmos()
    {
        //Gizmos.color = new Color(1, 1, 1, 0.2f);
        //for (int i = -100; i < 100; i++)
        //{
        //    Gizmos.DrawLine(new Vector3(i * 2 + 1, 0, -200), new Vector3(i * 2 + 1, 0, 200));
        //    Gizmos.DrawLine(new Vector3(-200, 0, i * 2 + 1), new Vector3(200, 0, i * 2 + 1));
        //}
        if(mst != null)
        {
            foreach (var room in generatingRooms) DrawRoomGizmos(room);

            int i=0, j=0;
            Gizmos.color = Color.cyan;
            mst.Traverse(n => { n.Traverse( child => { if (n != child) DrawGizmosLineToChild(n.Value, i, child.Value, ++j); }, 1); i++; });
            i =0;
            mst.Traverse(mst, n => UnityEditor.Handles.Label(n.Value.Root.position, $"Room: {i++}"));
        }
    }
#endif
}

﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


Shader "MrRogueBot/WarTableLineRender"
{
	Properties
	{
		_Color("Color", Color) = (1, 1, 1, 1)
		_Direction("Direction", Int) = 0
		_MyRange("My Range", float) = 40.0

	}

		//-------------------------------------------------------------------------------------------------------------------------------------------------------------
		CGINCLUDE

		// Pragmas --------------------------------------------------------------------------------------------------------------------------------------------------
#pragma target 3.0

// Includes -------------------------------------------------------------------------------------------------------------------------------------------------
#include "UnityCG.cginc"

// Structs --------------------------------------------------------------------------------------------------------------------------------------------------
struct VertexInput
	{
		float4 vertex : POSITION;
	};

	struct VertexOutput
	{
		float4 vertex : SV_POSITION;
		float4 worldpos : TEXCOORD0;
	};

	// Globals --------------------------------------------------------------------------------------------------------------------------------------------------
	float4 _Color;
	int _Direction;
	float _MyRange;

	// MainVs ---------------------------------------------------------------------------------------------------------------------------------------------------
	VertexOutput MainVS(VertexInput i)
	{
		VertexOutput o;
#if UNITY_VERSION >= 540
		o.vertex = UnityObjectToClipPos(i.vertex);
#else
		o.vertex = UnityObjectToClipPos(i.vertex);
#endif
		o.worldpos = mul(unity_ObjectToWorld, i.vertex);

		return o;
	}

	// MainPs ---------------------------------------------------------------------------------------------------------------------------------------------------
	float4 MainPS(VertexOutput i) : SV_Target
	{
		float4 vColor = _Color.rgba;

		if(_Direction == 0) {
			vColor.a *= (sin((_Time.w - i.worldpos.x * _MyRange) - cos(_Time.w - i.worldpos.x * _MyRange)) + 1) * 0.5;
		}
		else if (_Direction == 1) {
			vColor.a *= (sin((_Time.w + i.worldpos.x * _MyRange) - cos(_Time.w + i.worldpos.x * _MyRange)) + 1) * 0.5;
		}
		else if (_Direction == 2) {
			vColor.a *= (sin((_Time.w - i.worldpos.z * _MyRange) - cos(_Time.w - i.worldpos.z * _MyRange)) + 1) * 0.5;
		}
		return vColor.rgba;
	}

		// MainPs ---------------------------------------------------------------------------------------------------------------------------------------------------
	float4 SeeThruPS(VertexOutput i) : SV_Target
	{
		float4 vColor = _Color.rgba;// *(sin(_Time.w - i.worldpos.z * 100) + 1) * 0.5;

		//vColor.a *= (sin(_Time.w + i.vertex.y/20) + 1) * 0.5;


		return vColor.rgba;
	}

		ENDCG

	SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
			LOD 100

			// Behind Geometry ---------------------------------------------------------------------------------------------------------------------------------------------------
		/*Pass
		{
			// Render State ---------------------------------------------------------------------------------------------------------------------------------------------
			Blend SrcAlpha OneMinusSrcAlpha
			Cull Off
			ZWrite Off
			ZTest Greater

			CGPROGRAM
				#pragma vertex MainVS
				#pragma fragment SeeThruPS
			ENDCG
		}*/

		Pass
		{
			// Render State ---------------------------------------------------------------------------------------------------------------------------------------------
			Blend SrcAlpha OneMinusSrcAlpha
			Cull Off
			ZWrite Off
			ZTest LEqual

			CGPROGRAM
				#pragma vertex MainVS
				#pragma fragment MainPS
			ENDCG
		}
	}
}

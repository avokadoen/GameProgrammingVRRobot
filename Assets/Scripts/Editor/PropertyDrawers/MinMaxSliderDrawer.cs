﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(MinMaxSliderAttribute))]
class MinMaxSliderDrawer : PropertyDrawer
{
    public override void OnGUI( Rect position, SerializedProperty property, GUIContent label )
    {
        Rect labelPos = new Rect(position) { width = EditorGUIUtility.labelWidth };
        float widthUnit = (position.width - labelPos.width)/5;
        Rect minValuePos = new Rect(position) {width = widthUnit, x = labelPos.xMax };
        Rect sliderPos = new Rect(position) {width = 3*widthUnit-8, x = minValuePos.xMax + 4};
        Rect maxValuePos = new Rect(position) {width = widthUnit, x = sliderPos.xMax + 4 };
        
        if (property.propertyType == SerializedPropertyType.Vector2)
        {
            Vector2 range = property.vector2Value;
            float min = range.x;
            float max = range.y;
            MinMaxSliderAttribute attr = attribute as MinMaxSliderAttribute;
            EditorGUI.LabelField(labelPos, label);

            EditorGUI.BeginChangeCheck();
            int indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;
            min = Mathf.Min(EditorGUI.FloatField(minValuePos, min), max);
            EditorGUI.MinMaxSlider(sliderPos, ref min, ref max, attr.min, attr.max);
            max = Mathf.Max(EditorGUI.FloatField(maxValuePos, max), min);
            EditorGUI.indentLevel = indent;
            if (EditorGUI.EndChangeCheck())
            {
                range.x = min;
                range.y = max;
                property.vector2Value = range;
            }
        }
        else if (property.propertyType == SerializedPropertyType.Vector2Int)
        {
            Vector2Int range = property.vector2IntValue;
            float min = range.x;
            float max = range.y;
            MinMaxSliderAttribute attr = attribute as MinMaxSliderAttribute;
            EditorGUI.LabelField(labelPos, label);

            EditorGUI.BeginChangeCheck();
            int indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;
            min = Mathf.Min(EditorGUI.IntField(minValuePos, (int)min), (int)max);
            EditorGUI.MinMaxSlider(sliderPos, ref min, ref max, attr.min, attr.max);
            max = Mathf.Max(EditorGUI.IntField(maxValuePos, (int)max), (int)min);
            EditorGUI.indentLevel = indent;

            if (EditorGUI.EndChangeCheck())
            {
                range.x = (int)min;
                range.y = (int)max;
                property.vector2IntValue = range;
            }
        }
        else
        {
            EditorGUI.LabelField(position, label, "MinMaxSlider only allowed on Vector2 and Vector2Int");
        }
    }
}
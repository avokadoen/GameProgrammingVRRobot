﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetTwitchChat : MonoBehaviour {

    public Text chatDisplay;

    [SerializeField] private string notConnectedMsg = "Not connected to twitch chat";
    private TwitchClient twitchClient;

	void Start () {
        twitchClient = FindObjectOfType<TwitchClient>();
	}
	
	void Update () {
		if(twitchClient == null)
        {
            twitchClient = FindObjectOfType<TwitchClient>();
        }
        if(twitchClient == null)
        {
            chatDisplay.text = notConnectedMsg;
        }
        else
        {
            chatDisplay.text = twitchClient.GetTwitchMessages();
        }
	}
}

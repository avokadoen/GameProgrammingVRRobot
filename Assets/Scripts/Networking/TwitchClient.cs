﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.ComponentModel;
using System.Net.Sockets;
using System.IO;
using UnityEngine.UI;

public enum TwitchMessageType
{
    Whisper,
    Chat
}

public class TwitchClient : MonoBehaviour {

    [SerializeField] private string usernameVariable         = "RogueBotTwitchUsernameAuth";
    [SerializeField] private string channelNameVariable      = "RogueBotChannelConnect";
    [SerializeField] private string tokenEnvironmentVariable = "TwitchBotAuth";
    [SerializeField] private string ircChannel = "irc.chat.twitch.tv";
    [SerializeField] private int ircPort = 6667;

    private string botUsername          = null;
    private string connectedChannelName = null;
    private string oauthToken           = null;

    private TcpClient client;
    private NetworkStream stream;
    private StreamReader reader;
    private StreamWriter writer;

    [SerializeField] private int maxMessages = 15;
    private List<string> twitchMessages;
    private GameManager gameManager;

    public static TwitchClient instance = null;

    void Start () {
        gameManager = FindObjectOfType<GameManager>();
        twitchMessages = new List<string>();

        botUsername = Environment.GetEnvironmentVariable(usernameVariable);
        connectedChannelName    = Environment.GetEnvironmentVariable(channelNameVariable);
        oauthToken              = Environment.GetEnvironmentVariable(tokenEnvironmentVariable);

        if(botUsername == null || connectedChannelName == null || oauthToken == null)
        {
            Debug.Log("Twitch variables not instantiated correctly in environment, not connecting to twitch");
            gameManager.connectedToTwitch = false;
            Destroy(this);
        }
        ConnectToChannel();
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(this);
    }

    void Update () {
        if (client == null || !client.Connected) // This check sometimes throws a null reference exception, something is not working as it should. maybe do !client?.Connected
        {
            Debug.Log("Disconnected, reconnecting..");
            ConnectToChannel();
        }

        gameManager.connectedToTwitch = true;
        ReadChat();
    }

    private void ConnectToChannel()
    {
        client = new TcpClient(ircChannel, ircPort);
        stream = client.GetStream();
        reader = new StreamReader(stream);
        writer = new StreamWriter(stream);

        writer.WriteLine("PASS " + oauthToken);
        writer.WriteLine("NICK " + botUsername);
        writer.WriteLine("USER " + botUsername + " 8 * :" + botUsername);
        writer.WriteLine("JOIN #" + connectedChannelName);
        writer.WriteLine("CAP REQ :twitch.tv/commands"); // To enable whispers
        writer.Flush();
    }

    private void ReadChat()
    {
        if(stream.DataAvailable)
        {
            var message = reader.ReadLine();
            Debug.Log(message);
            if (message.Contains("PRIVMSG"))
            {
                // Getting the user's username by splitting it from the string
                var splitPoint = message.IndexOf("!", 1);
                var userName = message.Substring(0, splitPoint);
                userName = userName.Substring(1);

                // Getting the user's message by splitting from the string
                splitPoint = message.IndexOf(":", 1);
                message = message.Substring(splitPoint + 1);
                string messageToChat = userName + ": " + message;
                Debug.Log(messageToChat);
                twitchMessages.Add(messageToChat);
            }
            else if (message.Contains("WHISPER"))
            {
                // Getting the user's username by splitting it from the string
                var splitPoint = message.IndexOf("!", 1);
                var userName = message.Substring(0, splitPoint);
                userName = userName.Substring(1);

                // Getting the user's message by splitting from the string
                splitPoint = message.IndexOf(":", 1);
                message = message.Substring(splitPoint + 1);
                string messageToChat = "(Whisper)" + userName + ": " + message;
                Debug.Log(messageToChat);
                twitchMessages.Add(messageToChat);
            }
            else if (message.Contains("PING"))
            {
                KeepConnectionAlive();
            }
        }
        while(twitchMessages.Count > maxMessages)
        {
            twitchMessages.RemoveAt(0);
        }
    }

    public void WriteMessage(string m)
    {
        writer.WriteLine("PRIVMSG #" + connectedChannelName + " :" + m);
        writer.Flush();
    }

    public void WriteWhisper(string m, string user)
    {
        writer.WriteLine("PRIVMSG #" + connectedChannelName + " :/w " + user + " " + m);
        writer.Flush();
    }

    private void KeepConnectionAlive()
    {
        // This works to keep connection alive, but should maybe do it some other way so it could work with other irc clients?
        writer.WriteLine("PONG :tmi.twitch.tv");
        writer.Flush();
    }

    public bool IsConnected()
    {
        return client != null && client.Connected;
    }

    public string GetTwitchMessages()
    {
        string returnMessages = "";
        foreach(string msg in twitchMessages)
        {
            returnMessages = returnMessages + msg + Environment.NewLine;
        }
        return returnMessages;
    }
}

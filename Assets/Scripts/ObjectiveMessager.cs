﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveMessager : MonoBehaviour {

    public int score;
    private GameManager gameManager;
	// Use this for initialization
	void Start () {
        gameManager = FindObjectOfType<GameManager>();
	}
	
    public void CountObjectiveForLevel()
    {
        if(gameManager != null)
            gameManager.AddScoreToGameObjective(score);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * KNOWN BUGS:
 * Name: Left and right hand missmatch
 * Description:
 * Booting up controllers after start might result in missmatch on left and right hand according to steamvr
 * 
 * TODO:
 * generate play area mesh to check collision with level bounds to make player unable to walk out of level
*/

[RequireComponent(typeof(PlayerState))]
public class PlayerInput : MonoBehaviour {

    public bool RightHanded = true;
    public float OffsetTeleportAngle = -30;
    public GameObject TeleportPreviewObjectPrefab;
    public GameObject TeleportPreviewLinePrefab;
    public float handsAoE;
    public LayerMask pickupAbleMask;
    public LayerMask playerMask;
    public int maxSlopeAngleTp;
    public Valve.VR.InteractionSystem.Hand leftHand;
    public Valve.VR.InteractionSystem.Hand rightHand;

    public GameObject twitchCanvasLeft;
    public GameObject twitchCanvasRight;

    private int teleportIndex;
    private Vector3 teleportDirection;
    private Quaternion teleportRayRotationOffeset;
    private Vector3 teleportRayScale;
    private LayerMask teleportIgnoreLayers;
    private Valve.VR.InteractionSystem.Player interSysPlayer;
    private GameObject TeleportPreviewObject;
    private GameObject TeleportPreviewLine;
    private GameObject VrCamera;
    private PlayerState playerState;

    // hand data
    private const int LeftHand = 0;
    private const int RightHand = 1;

    private Transform[] hands;
    private PhysicalObject[] heldObjects;
    private float[] GripCooldown;

    private float rightMenuCooldown = 0;
    private float leftMenuCooldown = 0;
    private bool activeLeftChat = false;
    private bool activeRightChat = false;
    
    void Start () {
       
        playerState = GetComponent<PlayerState>();
        interSysPlayer  = GetComponent<Valve.VR.InteractionSystem.Player>();

        // Find is slow, but is used more freely in player as there will only be one
        VrCamera = transform.Find("SteamVRObjects").Find("VRCamera").gameObject;
        if(VrCamera == null)
            Debug.LogError("could not find VrCamera, maybe you have changed the names?");
        

        hands               = new Transform[2];
        hands[LeftHand]     = transform.Find("SteamVRObjects").Find("LeftHand");
        hands[RightHand]    = transform.Find("SteamVRObjects").Find("RightHand");

        heldObjects             = new PhysicalObject[2];
        heldObjects[LeftHand]   = heldObjects[RightHand] = null;

        TeleportPreviewObject   = Instantiate(TeleportPreviewObjectPrefab);
        TeleportPreviewLine     = Instantiate(TeleportPreviewLinePrefab);

        if (RightHanded)    { teleportIndex = LeftHand; }
        else                { teleportIndex = RightHand; }

        teleportRayRotationOffeset  = Quaternion.Euler(90 + OffsetTeleportAngle, 0, 0);
        teleportRayScale            = new Vector3(0.05f, 0.05f, 1);
        GripCooldown                = new float[2];

        teleportIgnoreLayers = ~(pickupAbleMask | playerMask);

    }
	
	void Update () {

        RightHandInput();
        LeftHandInput();
        TeleportInput();

        
    }

    private void FixedUpdate()
    {
        heldObjects[LeftHand]?.OnHolding(hands[LeftHand]);
        heldObjects[RightHand]?.OnHolding(hands[RightHand]);
    }

    private void RightHandInput() {
        GripInput("RightGrip");
        TriggerInput("RightTrigger");
        RightMenuInput("RightMenu");
    }

    private void LeftHandInput()
    {
        GripInput("LeftGrip");
        TriggerInput("LeftTrigger");
        LeftMenuInput("LeftMenu");
    }

    private void LeftMenuInput(string buttonName)
    {
        leftMenuCooldown -= Time.deltaTime;
        if (leftMenuCooldown < 0)
            leftMenuCooldown = 0;
        if (Input.GetButtonDown(buttonName))
        {
            leftMenuCooldown = 0.5f;
            activeLeftChat = !activeLeftChat;
            twitchCanvasLeft.SetActive(activeLeftChat);
        }
    }

    private void RightMenuInput(string buttonName)
    {
        rightMenuCooldown -= Time.deltaTime;
        if (rightMenuCooldown < 0)
            rightMenuCooldown = 0;
        if (Input.GetButtonDown(buttonName))
        {
            rightMenuCooldown = 0.5f;
            activeRightChat = !activeRightChat;
            twitchCanvasRight.SetActive(activeRightChat);
        }
    }

    private void GripInput(string buttonName)
    {
        int sideIndex = FindWorkingIndex(buttonName);
        if (sideIndex == -1) return;


        if (0.5 < Input.GetAxisRaw(buttonName) && heldObjects[sideIndex] == null && GripCooldown[sideIndex] <= 0)
        {
            heldObjects[sideIndex] = FindNearestObject(Physics.OverlapSphere(hands[sideIndex].transform.position, handsAoE, pickupAbleMask), hands[sideIndex].transform);
            if (heldObjects[sideIndex] != null)
            {
                int otherSideIndex = 0;
                if (sideIndex == LeftHand)
                    otherSideIndex = 1;

                // check special case for weapons stabilising
                WeaponPhysicalObject weapon = heldObjects[sideIndex].GetComponent<WeaponPhysicalObject>();
                if(weapon != null && !weapon.IsTriggerColliding(hands[sideIndex].transform.position))
                {
                    if (weapon.IsDefaultColliding(hands[sideIndex].transform.position) &&
                             heldObjects[sideIndex] != heldObjects[otherSideIndex]) { } // continue
                    else
                    {
                        if (weapon.IsStabilizingColliding(hands[sideIndex].transform.position) && heldObjects[sideIndex] == heldObjects[otherSideIndex])
                        {
                            if(GripCooldown[sideIndex] <= 0)
                            {
                                GripCooldown[sideIndex] = 0.5f;
                                weapon.OnStabilizing(hands[sideIndex]);
                            }    
                        }
                        heldObjects[sideIndex] = null;
                        return;
                    }
                        
                }
                


                // check if other hand is already holding object and clean up
                if (heldObjects[sideIndex] == heldObjects[otherSideIndex])
                    heldObjects[otherSideIndex] = null;

                //heldObjects[sideIndex].OnDrop(hands[sideIndex].transform, new Vector3());
                heldObjects[sideIndex].OnPickUp(hands[sideIndex].transform);
                GripCooldown[sideIndex] = 0.5f;
            }


        }
        else if (0.5 < Input.GetAxisRaw(buttonName) && heldObjects[sideIndex] != null && GripCooldown[sideIndex] <= 0)
        {
            // record velocity of either right or left hand based on sideIndex
            Vector3 velocity = ((sideIndex == RightHand) ? interSysPlayer.rightHand.GetTrackedObjectVelocity() : interSysPlayer.leftHand.GetTrackedObjectVelocity());
            heldObjects[sideIndex].OnDrop(hands[sideIndex].transform, velocity);
            heldObjects[sideIndex].OnPlayerDrop(heldObjects[sideIndex].transform);
            heldObjects[sideIndex] = null;
            GripCooldown[sideIndex] = 0.5f;
        }

        GripCooldown[sideIndex] -= Time.deltaTime;
        if (GripCooldown[sideIndex] < 0)
            GripCooldown[sideIndex] = 0;
    }

    private void TriggerInput(string buttonName)
    {
        int sideIndex = FindWorkingIndex(buttonName);
        if (sideIndex == -1) return;

        if (0.5 < Input.GetAxisRaw(buttonName) && heldObjects[sideIndex] != null)
        {
            var weapon = heldObjects[sideIndex].GetComponent<WeaponPhysicalObject>();
            if (weapon != null)
            {
                weapon.OnActivate(hands[sideIndex].transform);
                if (weapon.didShoot)
                    HapticFeedbackReceived(hands[sideIndex]);
            }
        }
    }

    public void HapticFeedbackReceived(Transform hand)
    {
        if (hand == hands[LeftHand])
            leftHand.controller.TriggerHapticPulse(500);
        else if (hand == hands[RightHand])
            rightHand.controller.TriggerHapticPulse(500);
    }

    private void TeleportInput()
    {

        bool previewTeleport = false;

        if ((Input.GetButton("LeftTrackpadClick") && RightHanded) ||
                    (Input.GetButton("RightTrackpadClick") && !RightHanded))
        {
            RaycastHit hit = RayCastTeleportLocation();
            if (hit.collider != null && IsValidTPAngle(hit.normal))
            {      
                previewTeleport                             = true;
                TeleportPreviewObject.transform.position    = hit.point; 
                TeleportPreviewLine.transform.position      = hands[teleportIndex].position;

                Quaternion rayRotation  = new Quaternion();
                rayRotation             = hands[teleportIndex].rotation * teleportRayRotationOffeset;
                Vector3 v               = rayRotation.eulerAngles;
                TeleportPreviewLine.transform.rotation = Quaternion.Euler(v.x, v.y, 0);

                teleportRayScale.z = Mathf.Sqrt(Mathf.Pow((hit.point.z - hands[teleportIndex].position.z), 2) + Mathf.Pow((hit.point.y - hands[teleportIndex].position.y), 2));
                TeleportPreviewLine.transform.localScale = teleportRayScale;

            }
        }
        else if ((Input.GetButtonUp("LeftTrackpadClick") && RightHanded) ||
                (Input.GetButtonUp("RightTrackpadClick") && !RightHanded))
        {
            RaycastHit hit = RayCastTeleportLocation();
            if (hit.collider != null && IsValidTPAngle(hit.normal))
            {
                Vector3 posOffset = transform.position - VrCamera.transform.position;
                posOffset.y = 0;
                transform.position = hit.point + posOffset;
            }
        }

        TeleportPreviewObject.GetComponent<Renderer>().enabled = previewTeleport;
        TeleportPreviewLine.gameObject.transform.Find("Cube").GetComponent<Renderer>().enabled = previewTeleport;

    }

    private bool IsValidTPAngle(Vector3 normal)
    {
        float angle = Vector3.Angle(Vector3.up, normal);
        return angle > -maxSlopeAngleTp && angle < maxSlopeAngleTp; 
    }

    private PhysicalObject FindNearestObject(Collider[] phyColliders, Transform handTransform)
    {
        float bestSoFar = Mathf.Infinity;
        PhysicalObject nearest = null;
        foreach (Collider phyCollider in phyColliders)
        {
            float distanceMagnitude = Mathf.Abs((handTransform.position - phyCollider.transform.position).magnitude);
            if (distanceMagnitude < bestSoFar)
            {
                bestSoFar = distanceMagnitude;
                nearest = phyCollider.GetComponent<PhysicalObject>();
            }
        }
        return nearest;
    }


    private RaycastHit RayCastTeleportLocation()
    {
        teleportDirection = hands[teleportIndex].TransformDirection(Quaternion.Euler(OffsetTeleportAngle, 0, 0) * Vector3.down);
        RaycastHit firstHit;
        bool didHit = Physics.Raycast(hands[teleportIndex].position, teleportDirection, out firstHit, playerState.TeleportRange, teleportIgnoreLayers);
        RaycastHit hit = firstHit;
        if (!didHit)
        {
            RaycastHit secondHit;
            didHit = Physics.Raycast(hands[teleportIndex].position + (teleportDirection * playerState.TeleportRange), Vector3.down, out secondHit, interSysPlayer.eyeHeight * 2, teleportIgnoreLayers);
            if (didHit)
            {
                hit = secondHit;
            }
        }

        return hit;
    }

    private int FindWorkingIndex(string buttonName)
    {
        if (buttonName.Contains("Right"))
            return RightHand;

        else if (buttonName.Contains("Left"))
            return LeftHand;

        else
        {
            Debug.LogError("invalid buttonName in FindWorkingIndex");
            return -1;
        }
    }

    public void TeleportPlayer(Vector3 position)
    {
        transform.position = position;
    }
}

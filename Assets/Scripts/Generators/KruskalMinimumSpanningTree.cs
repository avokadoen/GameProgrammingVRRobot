﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;



using Node = GeneratingRoom;
using System;
/// <summary>
/// An implemention of Kruskal's algorithm for minimum spanning trees 
/// to deal with room connections in level generation, MST-s are useful 
/// in finding which rooms are neighbouring rooms allowing us to connect
/// said rooms with corridors
/// </summary>
public class KruskalMinimumSpanningTree
{
    public TreeNode<Node> minimumSpanningTree;

    HashSet<TreeNode<Node>> forrest;
    HashSet<Edge> edges = new HashSet<Edge>();
    private struct Edge : IComparable<Edge>
    {
        public Node p, q; //nodes defining this edge
        /// <summary>
        /// Weight evaluated as euclidean distance
        /// </summary>
        public float Weight => Vector2Int.Distance(p.Position, q.Position);
        public static bool operator ==(Edge a, Edge b) => (a.p == b.p && a.q == b.q) || (a.p == b.q && a.p == b.q);
        public static bool operator !=(Edge a, Edge b) => !(a == b);
        public override bool Equals(object obj) => ((Edge)obj) == this;
        public override int GetHashCode() => p.GetHashCode() + q.GetHashCode() + Weight.GetHashCode();
        public int CompareTo(Edge other) => Weight.CompareTo(other.Weight);
    }
    public KruskalMinimumSpanningTree(IEnumerable<Node> collection)
    {
        forrest = new HashSet<TreeNode<Node>>();
        foreach (Node n in collection)
            forrest.Add(new TreeNode<Node>( n ));
        
        
        for (int i = 0; i < forrest.Count; i++)
        {
            for (int j = 1; j < forrest.Count - i; j++)
            {
                edges.Add(new Edge() { p = collection.ElementAt(i), q = collection.ElementAt((i + j) % forrest.Count) } );
            }
        }
        RunAlgorithm();
    }

    private void RunAlgorithm()
    {
        SortedSet<Edge> unusedEdges = new SortedSet<Edge>(edges);
        Edge current = unusedEdges.Min;
        unusedEdges.Remove(current);
        int maxiter = unusedEdges.Count*4;
        int i = 0;
        while (unusedEdges.Count > 0 && i < maxiter)
        {

            TreeNode<Node> treeA = null, treeB = null;
            foreach (var tree in forrest)
            {
                if (tree.Contains(current.p))
                    treeA = tree;
                if (tree.Contains(current.q))
                    treeB = tree;
                if (treeA != null && treeB != null)
                    break;
            }
            if(treeA != treeB && treeA != null && treeB != null)
            {
                TreeNode<Node> n = treeA.GetNode(current.p);
                treeA.Attach(TreeNode<Node>.Rotate(treeB.GetNode(current.q)), n);
                forrest.Remove(treeB);
            }
            unusedEdges.Remove(current);
            current = unusedEdges.Min;
            i++;
        }
        minimumSpanningTree = forrest.ElementAt(0);
    }
}

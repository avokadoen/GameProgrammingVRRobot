﻿using UnityEngine;
using System.Runtime.InteropServices;
public static class PluginAPI{
    
    static PluginAPI()
    {
        SetLoggerCallback(msg => Debug.Log($"<color=#baddad><b>[Native]::</b></color>" + msg));
        //SetLoggerCallback(msg => { return; });  <--- suppress logger
        int random = Random();
    }

    internal delegate void LogCallback( string message );
#if UNITY_IPHONE && !UNITY_EDITOR
        [DllImport ("__Internal")]
        internal static extern void SetLoggerCallback(LogCallback logCallback);
#else
    [DllImport("PluginTest")]
    internal static extern void SetLoggerCallback( LogCallback logCallback );

    [DllImport("PluginTest")]
    internal static extern int Random();
#endif
}

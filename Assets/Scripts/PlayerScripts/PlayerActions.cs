﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Obsolete("will be removed soon", true)]
public class PlayerActions : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TeleportPlayer(Vector3 position)
    {
        transform.position = position;
    }

    // Instead of broadcasting message, currently using a OverlapSphere to only send to relevant agents nearby
    public void BroadcastSoundToAgents(float loudNess, float soundRange)
    {
        foreach(Collider agent in Physics.OverlapSphere(transform.position, soundRange * loudNess, LayerMask.NameToLayer("Agent")))
        {
            agent.GetComponent<UnitSensor>().ReceiveSound(transform.position, loudNess);
        }
    }
}

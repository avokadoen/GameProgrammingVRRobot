﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class RectExtensions
{
    public static bool Intersects(this RectInt a, RectInt b)
    {
        return !(a.xMin > b.xMax || b.xMin > a.xMax || a.yMin > b.yMax || b.yMin > a.yMax);
    }

    public static bool Intersects( this RectInt a, RectInt b, int extraOffset )
    {
        return !(a.xMin > b.xMax + extraOffset || b.xMin > a.xMax + extraOffset || 
                 a.yMin > b.yMax + extraOffset || b.yMin > a.yMax + extraOffset );
    }
    public static bool ContainsInclusive(this RectInt a, Vector2Int point)
    {
        return (
            a.xMin <= point.x && a.xMax >= point.x &&
            a.yMin <= point.y && a.yMax >= point.y
            );
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelNode : ScriptableObject {
    // TODO: Use actual nodes instead of Vector2Ints for childs maybe?
    public bool visibleType;
    public Vector2Int position;
    public Vector2Int? parent;
    public Vector2Int? rightChild;
    public Vector2Int? leftChild;
    public Vector2Int? forwardChild;
    public LevelType levelType;


    public bool HasChilds()
    {
        if (rightChild   == null &&
            leftChild    == null &&
            forwardChild == null)
        {
            return false;
        }

        return true;
    }

    public bool IsAlreadyRelation(Vector2Int relation)
    {
        if (relation != rightChild &&
            relation != leftChild &&
            relation != parent)
        {
            return false;
        }

        return true;
    }

    public Vector2Int?[] GetChilds()
    {
        Vector2Int?[] returnChilds = new Vector2Int?[3];
        returnChilds[0] = rightChild;
        returnChilds[1] = leftChild;
        returnChilds[2] = forwardChild;

        return returnChilds;
    }
}

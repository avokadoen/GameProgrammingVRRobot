﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(Interactable))]
public class RegenerateLever : MonoBehaviour
{
    [System.Serializable] public class StringEvent : UnityEvent<string> { }

    public LinearMapping mapping;
    public VRKeys.Keyboard keyboard;

    [Range(0,1)]
    public float threshold;
    public StringEvent onHitThreshold;
    private void HandHoverUpdate(Hand hand)
    {
        if (mapping.value > threshold)
        {
            onHitThreshold.Invoke(keyboard.text);
            hand.HoverUnlock(GetComponent<Interactable>());
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadOcclusion : MonoBehaviour {
    public BlockVision  blockCameraVision;

    public LayerMask    mapLayer;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.layer != mapLayer)
        {
            return;
        }

        blockCameraVision.enabled = true;

    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.layer != mapLayer)
        {
            return;
        }

        blockCameraVision.enabled = false;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using System.IO;

public class Networking : MonoBehaviour
{
    public void Post( object postable ) 
    {
        StartCoroutine(Upload(postable));
    }
    
    private void Start()
    {
        PluginAPI.SetLoggerCallback(m => Debug.LogWarning(m));

        var fields = new Fields(){
        fields = new Package()
        {
            name = new StringValue() { stringValue = "Todd Howard" },
            motto = new StringValue() { stringValue = "It just Works(TM)" },
            lying = new BooleValue() { booleanValue = true }
        }
        };
        StartCoroutine(Upload(fields));
    }

    private IEnumerator Upload( object postable )
    {


        //MemoryStream memoryStream = new MemoryStream();
        //DataContractJsonSerializer ser = new DataContractJsonSerializer();
        //ser.WriteObject( memoryStream, 
        
        using (UnityWebRequest www = UnityWebRequest.Post("https://firestore.googleapis.com/v1beta1/projects/mrroguebot-6fc1b/databases/(default)/documents/user", JsonUtility.ToJson(postable)))
        {
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Upload complete!");
            }
        }
    }
}

    [DataContract]
    public class Fields
    {
        public Package fields;
    }
    
    [System.Serializable]
    public class Package
    {
        [DataMember(EmitDefaultValue = false)]
        public BooleValue somebool = null;
        public StringValue name;
        public StringValue motto;
        public BooleValue lying;
    }

    [System.Serializable] public class BooleValue { public bool? booleanValue = null; }
    [System.Serializable] public class StringValue  { public string stringValue = null; }

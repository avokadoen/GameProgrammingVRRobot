﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarTableNode : MonoBehaviour {

    public bool playedThrough = false;
    public bool activated = false;
    public LevelNode levelNode;

    public GameObject cylinder;
    public List<LineRenderer> lineRenderers;

    public void SelectNode()
    {
        activated = true;
        cylinder.GetComponent<Renderer>().material.SetColor("_Color", Color.cyan);
    }

    public void UnSelectNode()
    {
        activated = true;
        cylinder.GetComponent<Renderer>().material.SetColor("_Color", Color.blue);
    }

    public void ActivateNode()
    {
        activated = true;
        cylinder.GetComponent<Renderer>().material.SetColor("_Color", Color.blue);
    }

    public void DeActivateNode()
    {
        activated = false;
        cylinder.GetComponent<Renderer>().material.SetColor("_Color", Color.red); //( Possibly change this to orange ish?
    }

    public void PlayedThroughNode()
    {
        playedThrough = true;
        cylinder.GetComponent<Renderer>().material.SetColor("_Color", Color.green);
    }

    //TODO: Make it so the line renders the path chosen by the player as well
    public void UpdateOpenPaths()
    {
        foreach(LineRenderer line in lineRenderers)
        {
            line.GetComponent<Renderer>().material.SetColor("_Color", Color.cyan);
        }
    }

    public void UpdateClosePaths()
    {
        foreach(LineRenderer line in lineRenderers)
        {
            line.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
        }
    }
}

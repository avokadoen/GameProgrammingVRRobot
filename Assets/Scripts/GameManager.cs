﻿using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;
using UnityEngine.SceneManagement;

public struct LevelObjective
{
    public int goalScore;
    public int currentScore;
}

public class GameManager : MonoBehaviour {

    public LevelNode currentLevel;
    public LevelNode[,] nodeMapCopy;
    public Vector2Int lastCompletedLevelPosition;
    public List<LevelNode> completedNodes;
    public GameObject player;
    public GameObject instantiatedPlayerObject;
    public Vector3 playerSpawnPos;

    public LevelObjective levelObjective;
    public TwitchClient twitchClient;

    public int GameSceneBuildIndex;
    public int HubSceneBuildIndex;
    public string GameSceneName = "LevelScene";
    public string HubSceneName  = "Hub";

    public Random rand;
    [SerializeField] private string _seed;
    public string Seed { get { return _seed; } set { _seed = value; seedSet = true; } }

    public bool levelGenerated;
    public bool playerInstantiated;
    public bool seedSet                 = false;
    public bool gameSceneLoaded         = false;
    public bool warTableInstantiated    = false;
    public bool updatedWarTable         = false;
    public bool playerPositionSet       = false;
    public bool connectedToTwitch       = false;     

    public bool DEBUGINSTANTIATELEVEL = false; // only for debug through editor
    public bool DEBUGGOBACK = false;

    public static GameManager instance = null;

	// Use this for initialization
	void Start () {
        levelGenerated = false;
        /*GameSceneBuildIndex = SceneUtility.GetBuildIndexByScenePath("LevelScene");
        HubSceneBuildIndex = SceneUtility.GetBuildIndexByScenePath("HubScene");
        Debug.Log("LevelScene: " + GameSceneBuildIndex);
        Debug.Log("HubScene: " + HubSceneBuildIndex);*/
        if (player == null)
        {
            player = GameObject.Find("Player");
        }
        if(rand == null)
        {
            rand = new Random(_seed.GetHashCode());
        }
    }

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else if(instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void FixedUpdate () {
		if(SceneManager.GetActiveScene().name == GameSceneName)
        {
            if (!playerInstantiated)
            {
                instantiatedPlayerObject = Instantiate(player, new Vector3(0, 0, 0), Quaternion.identity);
                playerInstantiated = true;
                instantiatedPlayerObject.isStatic = true;
            }
            if (!levelGenerated)
            {
                LevelGenerator levelGenerator = GameObject.Find("LevelGenerator").GetComponent<LevelGenerator>();
                levelGenerator.playerReference = instantiatedPlayerObject;
                if (levelGenerator != null)
                {
                    levelGenerator.GenerateWithSeed(rand.Next(), currentLevel.levelType);
                    instantiatedPlayerObject.transform.position = playerSpawnPos;
                    instantiatedPlayerObject.isStatic = false; // TODO: This should be mitigated by making player presence persistent through scenes
                    levelGenerated = true;
                }
            }
            if (levelObjective.currentScore >= levelObjective.goalScore)
            {
                CompletedLevel();
            }
        }
        else if(SceneManager.GetActiveScene().name == HubSceneName)
        {
            if(completedNodes.Count > 0 && !updatedWarTable)
            {
                WorldMapGenerator worldMapGenerator = FindObjectOfType<WorldMapGenerator>();
                worldMapGenerator.UpdateCompletedNodes(completedNodes);
                WarTableSelector warTableSelector = FindObjectOfType<WarTableSelector>();
                warTableSelector.currentNode = worldMapGenerator.GetWarTableNode(lastCompletedLevelPosition);
                warTableSelector.transform.rotation = Quaternion.identity;
                warTableSelector.transform.position = warTableSelector.currentNode.transform.position;
                updatedWarTable = true;
            }
        }

        if (DEBUGINSTANTIATELEVEL)
        {
            LoadNewLevel();
            DEBUGINSTANTIATELEVEL = false;
        }
        if (DEBUGGOBACK)
        {
            GameDone();
            DEBUGGOBACK = false;
        }
	}

    public void CompletedLevel()
    {
        lastCompletedLevelPosition = currentLevel.position;
        completedNodes.Add(currentLevel);
        currentLevel = null;
        GameDone();
    }

    public void LoadNewLevel()
    {
        if(!gameSceneLoaded && currentLevel != null && SceneManager.GetActiveScene().name == HubSceneName)
        {
            levelObjective.goalScore = 20;
            levelObjective.currentScore = 0;
            gameSceneLoaded = true;
            SceneManager.LoadScene(GameSceneName);
        }
    }

    public void GameDone()
    {
        levelGenerated = false;
        playerInstantiated = false;
        playerPositionSet = false;
        updatedWarTable = false;
        gameSceneLoaded = false;
        levelObjective.goalScore = 999;
        levelObjective.currentScore = 0;
        SceneManager.LoadScene(HubSceneName);
    }

    public void InstantiateRandom()
    {
        rand = new Random(_seed.GetHashCode());
    }

    public void SetPlayerPosition(HashSet<Vector2Int> tiles, Vector2Int offset)
    {
        Vector2 tile = tiles.GetRandom(rand);
        playerSpawnPos = new Vector3((tile.x * 2) + offset.x, 0, (tile.y * 2) + offset.y);
        playerPositionSet = true;
    }

    public void AddScoreToGameObjective(int score)
    {
        Debug.Log("Score added: " + score);
        levelObjective.currentScore += score;
    }

    public void HandleTwitchInput(string user, string input)
    {

    }
}

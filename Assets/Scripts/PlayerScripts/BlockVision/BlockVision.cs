﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockVision : MonoBehaviour {

    public Material blockVisionMaterial;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, blockVisionMaterial);
    }
}

﻿Shader "MrRogueBot/WarTableScreen" {
	Properties {
		_Color("Color", Color) = (1,1,1,1)
		_HighlightColor ("Highlight Color", Color) = (1,1,1,1)
		[NoScaleOffset]
		_MainTex ("Albedo (RGB)", 2D) = "white" {}		
		[Normal]
		[NoScaleOffset]
		_BumpMap("Bumpmap", 2D) = "bump" {}
		[NoScaleOffset]
		_SpecularMap("Specularity (A)", 2D) = "white" {}
		[NoScaleOffset]
		_ParallaxMap("Parallax (A)", 2D) = "white" {}
		_UV_ScaleOffset("Tiling & Offset", Vector) = (1,1,0,0)
		_Parallax("Parallax", Range(0,0.1)) =  0.5
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows vetex:vert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		struct Input {
			float2 uv_MainTex;
			float3 viewDir;
		};

		sampler2D _MainTex;
		sampler2D _BumpMap;
		sampler2D _SpecularMap;
		sampler2D _ParallaxMap;
		half _Parallax;
		half _Glossiness;
		half _Metallic;
		float4 _UV_ScaleOffset;

		fixed4 _Color;
		fixed4 _HighlightColor;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void vert(inout appdata_full v) {
			v.texcoord = v.texcoord * _UV_ScaleOffset;
		}

		half saw(half value) {
			return value - floor(value);
		}
		half tri(half p, half value) {
			return 2 * abs(value / p - floor(value / p + 0.5));
		}

		void surf (Input IN, inout SurfaceOutputStandard o) {

			float2 parallaxOffset = ParallaxOffset(tex2D(_ParallaxMap, IN.uv_MainTex*_UV_ScaleOffset.xy + _UV_ScaleOffset.zw).r, _Parallax, IN.viewDir);
			

			fixed4 tex = tex2D(_MainTex, IN.uv_MainTex * _UV_ScaleOffset.xy + _UV_ScaleOffset.zw + parallaxOffset);
			

			fixed3 nrm = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex*_UV_ScaleOffset.xy + _UV_ScaleOffset.zw + parallaxOffset));
			fixed4 spec = tex2D(_SpecularMap, IN.uv_MainTex * _UV_ScaleOffset.xy + _UV_ScaleOffset.zw + parallaxOffset);

			//half highlight = tex.r * tri(10*(sin(_Time.w*0.01 + IN.uv_MainTex.y)+4), _Time.w + IN.uv_MainTex.y);//cos(tex.r + _Time.x + IN.uv_MainTex.y));
			//half highlight = tex.r * tri(0.5, tri(IN.uv_MainTex.y, sin(_Time.x + IN.uv_MainTex.x)));//cos(tex.r + _Time.x + IN.uv_MainTex.y));
			//-|2(x-.5)|+1
			//half highlight = tex.r * tri(0.5, tri(-(IN.uv_MainTex.y), sin(_Time.x + IN.uv_MainTex.x)));//cos(tex.r + _Time.x + IN.uv_MainTex.y));
			half midRamp = tri(1, IN.uv_MainTex.y);
			half highlight = nrm.b * (
				pow(tex.r * tri(1, -_Time.x*0.9 + IN.uv_MainTex.y * 5), 2) +
				pow(tex.b * tri(3, _Time.x + midRamp * 5), 2) +
				pow(tex.b * tri(2.6667, _Time.y + IN.uv_MainTex.y), 10) +
				pow(tex.g * tri(2.6667, _Time.x + IN.uv_MainTex.x), 2) +
				pow(tex.r * tri(4.6667, -_Time.y - IN.uv_MainTex.x), 10)
				)/5;
		
			half light = step(0.2, (tex.r + tex.g + tex.b) / 3);

			highlight *= smoothstep(0.8, 0.1, length(IN.uv_MainTex - 0.5));
//				+ tri(1.6667, _Time.x*1.1 + IN.uv_MainTex.y)
//				+ tri(1.6667, _Time.x*1.4 + IN.uv_MainTex.y)
//				+ tri(1, -_Time.x*0.9 + IN.uv_MainTex.x) 
//				+ tri(2, -_Time.x*0.45 + IN.uv_MainTex.x);//(1 - abs());

			//float2 uv = float2(frac(i.uv.x + cos((i.uv.y + _Time.x) * 100) / 3333.333), frac(i.uv.y));

			//o.Albedo = _Color * (1-highlight) + _HighlightColor * highlight;
			o.Albedo = light*(_Color * (1 - highlight) + _HighlightColor * highlight);

			o.Normal = nrm;
			// Metallic and smoothness come from slider variables
			o.Metallic = (1 - spec)* _Metallic;
			o.Smoothness = spec * _Glossiness;
			o.Alpha = tex.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}

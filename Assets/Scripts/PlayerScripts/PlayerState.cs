﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Class for holding and in some cases updating player state
/// 
/// health is a temporary solution as player is planned to contain physical object skeleton/body
/// </summary>
public class PlayerState : MonoBehaviour {

    // TODO: interface properly (non public)
    public float TeleportRange  = 2.0f;
    public float health         = 100; // central health 

    [SerializeField] private float dmgThreshold;

    public UnityEvent playerKilled;

    public void OnBodyPartCollisionEnter(Collision collision)
    {
        var physicalObject = collision.gameObject.GetComponent<PhysicalObject>();

        if(physicalObject == null)
            return; // for now, player only takes damage from objects of type physicalObject
        

        float otherDensity = physicalObject.Density;
        if (otherDensity <= 0) return;
        float potentialDamage = otherDensity * collision.relativeVelocity.magnitude;
        if (potentialDamage > dmgThreshold)
        {
            health -= potentialDamage;
        }

        if (health <= 0) playerKilled.Invoke();

    }

    public void OnBodyPartTriggerEnter(Collider other)
    {
        var physicalObject = other.gameObject.GetComponent<PhysicalObject>();

        if (physicalObject == null)
            return; // for now, player only takes damage from objects of type physicalObject

        float otherDensity = physicalObject.Density;
        if (otherDensity <= 0) return;
        float potentialDamage = otherDensity * physicalObject.body.velocity.magnitude * 0.95f; // HACK: we reduce it by 5% as its not a relative collision
        if (potentialDamage > dmgThreshold)
        {
            health -= potentialDamage;
        }

        if (health <= 0) playerKilled.Invoke();

    }


}

﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

public class LayerMaskTestTool : ScriptableWizard
{
    public LayerMask layerMask;
    [MenuItem("Tools/LayerMaskTool")]
    static void ShowWindow() 
        => DisplayWizard<LayerMaskTestTool>("LayerMaskTool", "Copy To Clipboard");
    void OnWizardCreate() 
        => EditorGUIUtility.systemCopyBuffer = layerMask.value.ToString();
}
#endif
﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(RoomType.SpawnListItem))]
class SpawnListItemDrawer : PropertyDrawer
{
    Texture2D cachedItemPreview;
    public override float GetPropertyHeight( SerializedProperty property, GUIContent label )
    {
        return EditorGUIUtility.singleLineHeight * 3 + 8;
    }
    public override void OnGUI( Rect position, SerializedProperty property, GUIContent label )
    {
        GUI.Box(position, GUIContent.none, "helpbox");
        Rect previewPos = new Rect(position) { width = position.height-8, height = position.height-8, x = position.x + 4, y = position.y + 4 };
        Rect parameterRect = new Rect(position) {
            width = position.width - position.height,
            x = previewPos.xMax,
            height = EditorGUIUtility.singleLineHeight,
            y = position.y + 4
        };
        

        SerializedProperty 
            spawnLimit = property.FindPropertyRelative("spawnLimits"), 
            itemRef = property.FindPropertyRelative("spawnable"),
            angleIter = property.FindPropertyRelative("angleIteration");


        if ( itemRef?.objectReferenceValue != null)
        {
            //if(cachedItemPreview == null) {
            if(Event.current.type == EventType.Repaint)
            {
                cachedItemPreview = AssetPreview.GetAssetPreview(((Spawnable)itemRef.objectReferenceValue).gameObject);
            }
            if (GUI.Button(previewPos, cachedItemPreview, "AnimationKeyframeBackground"))
            {
                EditorGUIUtility.PingObject(itemRef.objectReferenceValue);
                if(Event.current.button == 1)
                {
                    GenericMenu itemContexMenu = new GenericMenu();
                    itemContexMenu.AddItem(new GUIContent("Select Spawnable"), false,
                        () => Selection.SetActiveObjectWithContext(itemRef.objectReferenceValue, property.serializedObject.targetObject)
                    );
                    itemContexMenu.DropDown(previewPos);
                }
            }
        }

        EditorGUI.BeginChangeCheck();
        EditorGUI.indentLevel = 0; //TODO validate that this don't need to be reset back
        EditorGUIUtility.labelWidth -= 36;

        EditorGUI.ObjectField(parameterRect, itemRef, typeof(Spawnable));

        parameterRect.y = parameterRect.yMax; //shift rect downwards
        EditorGUI.PropertyField(parameterRect, spawnLimit);

        parameterRect.y = parameterRect.yMax; //shift rect downwards
        EditorGUI.PropertyField(parameterRect, angleIter);

        EditorGUIUtility.labelWidth += 36;


        if (EditorGUI.EndChangeCheck())
        {
        }
    }

}
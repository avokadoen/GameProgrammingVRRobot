﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


Shader "MrRogueBot/TableModelShader"
{
	Properties
	{
		_Color( "Color", Color ) = ( 1, 1, 1, 1 )
	}

	//-------------------------------------------------------------------------------------------------------------------------------------------------------------
	CGINCLUDE
		
		// Pragmas --------------------------------------------------------------------------------------------------------------------------------------------------
		#pragma target 3.0

		// Includes -------------------------------------------------------------------------------------------------------------------------------------------------
		#include "UnityCG.cginc"

		// Structs --------------------------------------------------------------------------------------------------------------------------------------------------
		struct VertexInput
		{
			float4 vertex : POSITION;
		};
		
		struct VertexOutput
		{
			float4 vertex : SV_POSITION;
			float4 worldpos : TEXCOORD0;
		};

		// Globals --------------------------------------------------------------------------------------------------------------------------------------------------
		float4 _Color;
				
		// MainVs ---------------------------------------------------------------------------------------------------------------------------------------------------
		VertexOutput MainVS( VertexInput i )
		{
			VertexOutput o;
#if UNITY_VERSION >= 540
			o.vertex = UnityObjectToClipPos(i.vertex);
#else
			o.vertex = UnityObjectToClipPos(i.vertex);
#endif

			o.worldpos = mul(unity_ObjectToWorld, i.vertex);
			return o;
		}
		
		// MainPs ---------------------------------------------------------------------------------------------------------------------------------------------------
		float4 MainPS( VertexOutput i ) : SV_Target
		{
			float4 vColor = _Color.rgba;
			vColor.a = (sin((_Time.w + i.worldpos.y * 50) - cos(_Time.w + i.worldpos.y * 50) + i.worldpos.x + i.worldpos.z) + 1) * 0.2;
			return vColor.rgba;
		}

		// MainPs ---------------------------------------------------------------------------------------------------------------------------------------------------
		float4 SeeThruPS( VertexOutput i ) : SV_Target
		{
			//float4 vColor = (1, 1, 1, 0);
			float4 vColor = _Color.rgba * (sin((_Time.w + i.worldpos.y * 50) - cos(_Time.w + i.worldpos.y * 50) + i.worldpos.x + i.worldpos.z) + 1) * 0.5;
			//vColor.a *= 0.9;// (sin(_Time.w + i.vertex.y / 20) + 1) * 0.5;


			return vColor.rgba;
		}

	ENDCG

	SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		LOD 100

		// Behind Geometry ---------------------------------------------------------------------------------------------------------------------------------------------------
		/*Pass
		{
			// Render State ---------------------------------------------------------------------------------------------------------------------------------------------
			Blend SrcAlpha OneMinusSrcAlpha
			Cull Off
			ZWrite Off
			ZTest Greater

			CGPROGRAM
				#pragma vertex MainVS
				#pragma fragment SeeThruPS
			ENDCG
		}*/

		Pass
		{
			// Render State ---------------------------------------------------------------------------------------------------------------------------------------------
			Blend SrcAlpha OneMinusSrcAlpha
			Cull Off
			ZWrite Off
			ZTest LEqual

			CGPROGRAM
				#pragma vertex MainVS
				#pragma fragment MainPS
			ENDCG
		}
	}
}

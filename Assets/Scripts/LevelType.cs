﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Level Type", menuName = "Level Type")]
public class LevelType : ScriptableObject {

    public GameObjective gameObjective;
    public GameObject warTablePrefab;
    public LevelInfo levelInfo;
    public int spawnFromStage;
    [Range(0, 20)]
    [Tooltip("Spawn rating is per stage")]
    public int[] spawnRating;

    //---   Different game objectives on each level ---//
    [System.Serializable]
    public enum GameObjective  // Could possibly be made into a scriptable object if we need that
    {
        killEnemies,
        reachGoal,
        unlockRoom,
        solvePuzzle,
        enablePower,
        collectObjects,
        surviveWaves,
        killBoss,
        restRoom
    }

    //---   Data for the LevelGenerator ---//
    [System.Serializable]
    public struct LevelInfo
    {
        public Vector2Int levelGridSize;

        [MinMaxSlider(1, 36)]
        public Vector2Int roomsToGenerate;

        [Range(0, 80)]
        public int roomsMaxSize;

        [Range(1, 10)]
        [Tooltip("Lower value is greater chance")]
        public int roomSpawnOdds;

        public RoomType[] roomTypes;

        public int minRooms => roomsToGenerate.x;
        public int maxRooms => roomsToGenerate.y;
    }

}

﻿using System;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(LabelAsAttribute))]
public class LabelAsDrawer : PropertyDrawer
{
    public override void OnGUI( Rect position, SerializedProperty property, GUIContent label )
    {
        try
        {
            var propertyAttribute = this.attribute as LabelAsAttribute;
            
            if (!property.isArray)
            { label.text = propertyAttribute.text; }
            else
            {
                Debug.LogWarningFormat(
                    "{0}(\"{1}\") doesn't support arrays ",
                    typeof(LabelAsAttribute).Name,
                    propertyAttribute.text
                );
            }
            EditorGUI.PropertyField(position, property, label);
        }
        catch (System.Exception ex) { Debug.LogException(ex); }
    }
}
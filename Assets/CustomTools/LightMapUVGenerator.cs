﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;

public static class LightMapUVGenerator
{
    [MenuItem("Tools/GenerateLightMapUVs")]
    static void GenerateUVs()
    {
        Unwrapping.GenerateSecondaryUVSet(Selection.activeGameObject.GetComponent<MeshFilter>().sharedMesh);
    }
    [MenuItem("Tools/GenerateLightMapUVs", validate = true)]
    static bool ValidateCanGenerateUVs() => (Selection.activeGameObject?.GetComponent<MeshFilter>()?.sharedMesh != null);
}
#endif
﻿Shader "Hidden/BVShader"
{
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM

			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = (0, 0, 0, 1);
				// just invert the colors
				return col;
			}
			ENDCG
		}
	}
}
